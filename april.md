---
title: 棄蒼初見紀念日12h產糧活動
layout: post
date: 2024-03-08
lang: zh-Hant
---

📢📢鄉親們看過來啊！棄蒼初見16週年紀念節目單來囉~~

#### 爲什麼是4月5日呢

1. 因爲這一天是棄蒼劇中初見的日子！（好吧我也沒數過，反正《青宮客》裏這麼說的）

2. 因爲這一天忌結親、嫁娶、入宅、求嗣，總之不適合戀愛，但是非常適合棄蒼這對強取豪奪貓毛亂飛的cp

3. 因爲這一天在清明後，誰家正常cp清明節辦活動啊！但棄蒼不是正常cp啊！

#### 節目單

##### 07:00  [致五千年後的你](../after5000years/0/) By Chaconne

[http://www.36rain.com/read.php?tid=159009](http://www.36rain.com/read.php?tid=159009)

##### 08:00  [道士和親](../fate/73/) By 終朝採藍

[http://www.36rain.com/read.php?tid=155858](http://www.36rain.com/read.php?tid=155858)

##### 09:00  [契約詩](../contract_poem/0/) By dgrey

[http://www.36rain.com/read.php?tid=158826](http://www.36rain.com/read.php?tid=158826)

##### 10:00  [ETERNAL CAGE](../eternal_cate/0/) By prayer

[https://www.bilibili.com/video/BV1XZ421q7No/](https://www.bilibili.com/video/BV1XZ421q7No/)

##### 11:00  戀愛試用期 By 寒鴉知我意

[https://weibo.com/5793078975/O8hSMcYV4](https://weibo.com/5793078975/O8hSMcYV4)

##### 12:00  棄蒼cp圖 By 小雨

[https://xiaoyudongchenglexiaoxue.lofter.com/post/1e8a1970_2bb8cd3a2](https://xiaoyudongchenglexiaoxue.lofter.com/post/1e8a1970_2bb8cd3a2)

##### 13:00  貓想要，貓得到 By 章魚腿毛

[https://weibo.com/2411235383/O8iFsBExJ](https://weibo.com/2411235383/O8iFsBExJ)

##### 14:00  兩相疑 By 蓬山

[http://www.36rain.com/read.php?tid=157199](http://www.36rain.com/read.php?tid=157199)

##### 15:00  堂前燕 By 扶桑

[http://www.36rain.com/read.php?tid=159012](http://www.36rain.com/read.php?tid=159012)

##### 16:00  傳說 By 幾時修得到海棠

[https://www.bilibili.com/video/BV1px421D7HW/](https://www.bilibili.com/video/BV1px421D7HW/)

##### 17:00  囚徒 By 三千白雪三千塵

[https://weibo.com/5876827836/O8likeqDR](https://weibo.com/5876827836/O8likeqDR)

##### 18:00  1001夜衍生圖 By niudanzhi

[https://qicang.ovh/anniversary16/0/#1001夜](https://qicang.ovh/anniversary16/0/#1001夜)

##### 19:00  棄蒼cosplay By 玄攸

[https://weibo.com/6451018818/O8l1EBuwy](https://weibo.com/6451018818/O8l1EBuwy)

##### 20:00  棄蒼情頭 By 十月

[https://weibo.com/7737402078/O8lq7m8Wl](https://weibo.com/7737402078/O8lq7m8Wl)

##### 21:00  天命衍生圖 By niudanzhi

[https://qicang.ovh/anniversary16/0/#天命衍生圖](https://qicang.ovh/anniversary16/0/#天命衍生圖)

##### 22:00  小龍人棄摸魚頁 By niudanzhi

[https://qicang.ovh/anniversary16/0/#小龍人棄摸魚頁](https://qicang.ovh/anniversary16/0/#小龍人棄摸魚頁)

##### 23:00  老年人日記衍生 By niudanzhi

[https://qicang.ovh/anniversary16/0/#老年人日記衍生](https://qicang.ovh/anniversary16/0/#老年人日記衍生)

##### 24:00  睡美人新編 By niudanzhi

[http://www.36rain.com/read.php?tid=159016](http://www.36rain.com/read.php?tid=159016)

##### 彩蛋  問心 By 容予

[http://www.36rain.com/read.php?tid=159018](http://www.36rain.com/read.php?tid=159018)