---
author: 三層梳妝臺
title: 棄蒼初見16週年紀念 （圖集）
date: 2023-07-16 00:00:11
layout: post
depth: 1
lang: zh-Hant
category: anniversary16
toc: true
---

#### By 三層梳妝臺

---

### 1001夜

18:00

<figure class="responsive-figure">
    <img src="../1712311458.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<figure class="responsive-figure">
    <img src="../1712311458_2.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<br/>

### 天命衍生圖

21:00

<figure class="responsive-figure">
    <img src="../1712317250_7-2.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<figure class="responsive-figure">
    <img src="../1712317251_7.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<br/>

### 小龍人棄摸魚頁

22:00

<figure class="responsive-figure">
    <img src="../1712317349.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<br/>

###  老年人日記衍生

23:00

<figure class="responsive-figure">
    <img src="../1712317405_12.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>
