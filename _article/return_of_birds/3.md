---
title: 倦鳥之還
date: 2018-01-01 3:00:01
layout: post
lang: zh-Hant
category: birds
depth: 2
---

### 3.

真是……自大又狂妄。

蒼重新打開手機，確如棄天帝所說，電量沒有變化。屏幕亮起來的白色熒光映出他的眼睛，他一時竟不知道該做些什麼。

在萬年牢裏，他有太多時間可以揮霍了。沒來得及看完的書，只通關到一半的遊戲，沒有整理的數據，沒有聽完的錄音，他現在可以慢慢地做這些事，也不必再惜字如金，想記多少日記就記多少日記，反正都帶不走，反正都是消遣。

棄天帝大概不知道給他這種自由意味着什麼。

蒼點開一段音頻，在呼嘯的風聲裏閉上眼睛。

他曾數次造訪北越天海，不敢說對那裏的懸崖峭壁以及棲息在懸崖峭壁上的海鳥們瞭如指掌，但至少比普通人知道得更多一些。海浪拍打礁石，成群的海鸚遮天蔽日，它們高聲吟唱，鳴叫此起彼伏。

這個文件來自多年以前，可以算是翠山行留給他的遺物之一。他們原計劃追蹤一隻編號爲1354的剪水鸌樣本，那天風太大，1354一飛走，GPS就失了蹤跡。翠山行說沒關係，它晚上還會回來，我可以再安一個試試。蒼堅決不允許，說晚上太危險了，翠山行說可是錯過了今晚，就還要再等一年。

蒼後來痛心疾首地想，他那天就應該鎖住翠山行不讓他出門，再等一年又如何，他還那麼年輕，1354都比他活得長久，數據哪有師弟的命重要。

但是他發現時已經晚了。蒼和一步蓮華找了整整三天，也沒有找到翠山行到底在哪裏。他的追蹤系統傳回1354的運動軌跡，它穿過風與浪，在漩渦中心尋找食物，蒼可以想象它微微側身飛行，翅膀劃開水面，銳利，冷靜，像一把凜冽的手術刀果決地刺入皮膚深處。

可是翠山行再也沒有回來了。他應該沉睡在懸崖與海浪之下，化成一隻斷翅的鳥，向深處墜落、墜落、再墜落。夜半時分走到營地門口時一步蓮華說你難過就哭出來吧，哭出來可能好一點。蒼搖搖頭，撐着膝蓋乾嘔，胃酸浸潤到喉管，像有火在燒，但眼眶始終乾澀，始終沒有流淚。

他們踏上的是一條少有人走的路。海鳥自由而神祕，帶着卓越、孤獨、矛盾等迷人的特質在多種自然環境之間切換。蒼永遠記得翠山行戴着耳機分辨鸌形目鳥類鳴聲時歡呼雀躍的樣子，他說蒼師兄，你聽啊，它們好像在唱一首詩。

那首詩現在迴盪在萬年牢裏，是會讓人心安也會讓人心碎的旋律。

海潮聲浪滾滾，蒼翻看通訊錄，好多人和他在同一條船上，如果棄天帝只留下了他，那意味着其他的同伴們已無生機。他不是技術工種，無法判斷爲什麼會遭遇這些，在此之前這艘船已經行過了千萬裏航路，冰海中歸來亦安然無恙。被人喚做狼叔的輪機長豪情萬丈，笑說十七級風也不怕，就怕你們到時吐得找不着北哦。

是因爲單純的暴雨嗎？抑或其中有神的憤怒推波助瀾？這次航行也有群，打開聊天框就看見氣象組的紫宮太一和愁落暗塵在討論取樣地點，四非凡人偶爾插上兩句提提建議，六點半左右，輪班到後勤組的莫滄桑準時發來一個敲碗的表情喊大家出來領晚上的飯。然後就沒有然後了，所有人頭像都變成灰色，熱烈的討論止於風雨前夕。

當時沒有信號，現在依然沒有信號。蒼收到的最後一條信息來自赭杉軍，他發來一隻盤旋的信天翁，陽光很好，海上有風，背景的海岸顯示出他剛剛離開混沌巖池。

赭杉軍惜字如金，圖片底下算上標點符號也不會超過十個字。

他說蒼，我要來找你了。

蒼說好啊，我在北越天海等你。

那句回覆當時一直轉圈，一直髮不出去，現在也是如此。

蒼熄掉屏幕，腑臟攪動着發痛。也許這些疼痛來自棄天帝所說的治癒，不論如何，他都無法想象神怎麼把自己從海里撈上來，或者他更希望神找到的不是自己，而是他失去的那些同路人。

唯一幸運的是還好還有赭杉軍，以他們之間的默契，以當時的天氣，赭杉軍應該很容易判斷出他們的船遇到了什麼，但是他無法得知自己在這樣一個有悖常理的空間裏，又要怎麼找到自己呢。

蒼感到疲憊。他閉上眼睛，萬年牢很靜，只有自己的心跳和呼吸。
