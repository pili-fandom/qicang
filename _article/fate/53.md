---
title: 天命
author: 终朝采蓝
category: fate
layout: post
date: 2023-04-28 00:10:53
lang: zh-Hant
depth: 2
---

### 第53章

<br/>

蒼回到寢宮的內室時，武神散著一頭漆黑的長髮，斜靠榻上自斟淺飲。

也許是酒吧，蒼心想。

青燈中的燭幽幽地燃著，映襯金藍異瞳的寒芒更加幽深莫測。一時間誰都沒有說話，直到武神「噹」地一聲將手中的杯盞砸在道者腳邊的空地上，打破了沈默。白瓷碎了一地，濺起的清酒沾溼了道者樸素的衣襬。「哦？你回來做什麼？」

「殿下⋯⋯」

「哼⋯哈哈哈⋯⋯」

蒼長嘆一聲，「倘若你吾還有那麼一點情份，可否聽完蒼的請求⋯⋯」

「說。」武神簡潔道。

「放下吧。你吾退隱山林，找個人跡罕至的所在，吾陪你⋯⋯」

「哼⋯哈哈哈⋯⋯」武神突然笑出聲，冷冷地品味著道者臉上的神情，「現在說這句話，不覺得有點太遲嗎，蒼。」

「的確，是蒼之過。」道者長眉低垂，輕聲道，「若你不應，蒼能理解。蒼實在沒有資格要求你什麼⋯⋯但⋯⋯」

武神從榻上起身，神色冷酷地向前幾步逼近道者。強大的壓迫感撲面而來，蒼一動不動地站著，自顧說下去。

「若你答應，蒼會盡自己所能，補償人類對你的虧欠。」

「哈哈哈！又是這種什麼都想擔的愚蠢！」武神嘲弄地大笑幾聲，「虧欠？補償？怎麼補償？哈⋯⋯那群素未謀面的人類，與你何干？！」

「⋯⋯」蒼不語。靜立片刻，他向腰間伸去，緩緩解開交扣，抽下了淡紫色的腰封。

道者隨後向前一步，深紫的道袍無聲地落在身後的地面上。

武神冷冷地望著他，挑了挑眉，不發一言。

蒼再次向前一步，內袍也緩緩落了下來，露出光潔修長的身軀。就這樣一步一步上前，衣物次第脫落，直到站在武神面前。

武神的氣息更危險了，近乎一字一頓。

「你這是什麼意思？」

「蒼別無所長，只能⋯⋯盡力補償⋯⋯」道者低垂著細長的眉眼，嘴唇微動，輕聲地說。

「你這是什麼意思？！」

道者白皙優美的裸露身軀下一秒貼了上來，蒼突然抱住了他。

「殿下。」蒼很輕地說。

「你這是什麼意思？你這是什麼意思？！」武神茫然又煩躁地重複著。呼吸似乎停滯了一拍，胸口一團悶雷轟然炸裂，憤怒、情慾、不甘、困惑，無法言明的紛雜思緒被硬生生地挑起，讓他幾乎感到烈焰焚身的痛楚。曾經多麼期望蒼能主動地與他貼近，可不知怎麼，這似乎並不是他想要的。「為了一群不相干的人類，你要做到這個份上？」

「蒼能為有限⋯⋯」蒼垂著眼睛，並沒有看他，「若還有蒼能為之事⋯⋯你的心願⋯⋯蒼也盡力為你完成。」

「夠了！」武神一把推開貼在身前赤裸的道者，光滑細膩的觸感再一次毫無餘地地挑起了他的心火。

「啊⋯⋯」猝不及防地，道者被粗暴地一拽，雙手被舉過頭頂，按在了一側的山岩上。壓迫之下蒼不得不本能地貼近石壁，侵骨的寒冷刺透裸露的肌膚，道者不禁打了個寒顫。

「這可是你自找的⋯⋯」武神在道者耳畔輕聲說，低沈而危險的語調帶著一種近乎原始的野蠻。

「嗯。」蒼平靜地應了一聲。

<br/>

這次的擴張很潦草，武神似乎格外沒有耐心，蒼並不意外。身後高於人類體溫沸騰如焚，燒得他的後背幾乎有些疼痛。

灼熱的硬物長驅直入將身體的空缺填滿，隨後以懲罰式的力度抽動起來。蒼斷斷續續地隨著抽動的節奏低聲呻吟著，四肢百骸奔湧的魔氣讓他幾乎分不清痛楚與快感的邊界。

<br/>

「既然想要補償，那就好好承受吧。」武神在道者耳畔惡狠狠地說。他隨後手一揚，將道者粗暴地摔在榻上。

「啊⋯⋯」蒼被摔痛，輕輕地悶哼一聲。他試圖微微抬起上身，武神並未給他餘地，而是再一次起身向前，對著有些微微紅腫的穴口再一次毫不留情地插了進去。道者手臂環在他的腰間，在他身下發出破碎的呻吟，藍紫的眼瞳中盈滿水霧，有種鏡花水月般的朦朧，不知怎地，卻更加激起他的心火。

情慾之外，還有一種叫作「征服」的慾望。

一個再三忤逆他，而他卻對此無可奈何的人類。此刻想要征服這個人類的慾望比以往都要來的熱烈。蒼腿間不知何時起沾了淡淡的血跡，他並沒有察覺。此刻，彷彿只差一點點，他就能徹底征服這個叫「蒼」的人，讓他在自己身下呻吟、哭泣、求饒。

<br/>

在意識到蒼正在隨著他抽動的節奏，挺起腰笨拙地討好迎送的時候，武神惡狠狠地冷笑一聲，「吾允你自己動了嗎！」

「嗚⋯⋯」一隻有力的手掌扼住了蒼的咽喉，將他的上身按了回去，力度之大，蒼一時無法呼吸。下身的攻勢愈演愈烈，脖頸的力度也漸漸收緊，似乎勢要將道者脆弱的人類之軀撞的片甲不留。

忤逆他的人類，只有在他身下求饒的結局。

<br/>

「殿⋯⋯下⋯⋯」蒼止不住地發抖，只能發出斷斷續續的泣音。「吾⋯⋯無法⋯⋯呼⋯⋯吸⋯⋯了⋯⋯」

彷彿自夢中驚醒，武神猛地回神，突然鬆開了手。此刻他才驀然驚覺，道者白皙的脖頸上已經赫然留下一道觸目驚心的瘀青。而垂目望去，他們交合的下體已遍佈血跡。

<br/>

「你⋯⋯」武神垂著頭，沉著臉，從道者身體中退了出來，頹然坐在了一邊。

「你⋯⋯」他微張著雙唇，卻一句話都講不出。

從什麼時候起，他和蒼之間產生了無法修補的裂痕？

不該如此，他本意也並非如此。

他們始終踏在不同的軌跡上，也許真的只是短暫地交匯。孤獨的五百年太漫長，漫長到足以改變任何神人的心性。他變了嗎？而蒼，是不是也變了。

<br/>

被挑起的心火依然熊熊燃燒，武神還硬著，只是悶頭坐在一旁。蒼漸漸平復呼吸，溫和地抓住他的手。「無妨⋯⋯」

「夠了。」武神悶悶地打斷。

「吾說了，無妨。」蒼平靜地重複著，溫和而有力。

「⋯⋯」武神目光複雜地盯了道者片刻，生硬地命令道，「轉過身去。」

蒼順從地照做了。這種程度的疼痛，他經歷過太多，實在不值一提。「你進來便是。」

「腿，夾緊。」武神抬起道者的臀，對著腿根的縫隙插了進去。「別亂動，夾緊。」

「嗯⋯⋯」

<br/>

武神低沈地喘息著，不知抽動了多久，才將方才的慾望洩在道者腿根的夾縫裡。心火終於些許平息，他鬆開了道者，再次頹然跌坐在一旁。

<br/>

「消氣了嗎。」蒼翻過身來，從容地擦去雙腿上的污跡，神色平靜地問。

「哼。」武神頭一甩，扭到另一邊，悶悶地不肯說話。

「還在生氣？」蒼討好般地輕輕推了推他。

「哼！」

<br/>

見武神仍在生著悶氣，沒有回應的意思，蒼無奈地嘆了口氣。正思考對策，手臂被猛地一拽。武神把道者拉進了懷裡，輕輕擁住，語氣依舊有些發悶。

「吾，脾氣，不太好。」

「嗯⋯⋯你有資格生氣，是蒼之過。」蒼平靜地安慰道。

「吾，不喜歡，你為了一群不相干的人，忤逆吾。」

「抱歉。」蒼垂下眼睛。

「弄疼你了？看著吾⋯⋯」武神扳過道者清秀的面頰，細細撫過那修長脖頸間的瘀青，想為道者療復。只是驀地一愣，這時他才突然憶起，自己已經沒有再生之力了。

金藍異瞳一時有些茫然無措，「疼，疼嗎。」他垂下頭，瞥見蒼的下身還有尚未擦去的血跡。

——也無法療復了。

「疼，疼嗎⋯⋯」武神垂頭喪氣地重複問。

「無妨。」蒼搖搖頭。

武神隨後陷入了沈默，只是將道者愈抱愈緊。

<br/>

「殿下。可願與蒼退隱山林，從此不問世事？」被緊緊擁著，蒼輕聲道，「蒼會陪你做所有你想做的事，也會把蒼的過去，一五一十地說給你聽。」

武神緊緊地抱著他，指尖插進柔軟的淺色長髮，與道者額頭相貼，卻沒有回話。蒼無聲地嘆了口氣，閉上細長的眼眸，安靜地靠在武神懷裡。

<br/>

良久，武神打破了沈默。「吾答應你。」

蒼突然緊緊握住了他的手。

「吾答應你⋯⋯放下。」親吻過眉間的朱砂流紋，此時那裡如泣血般格外鮮紅。「蒼，吾答應你。魔界，以及魔龍，蒼，吾會給你一個交代⋯⋯」

燈影下道者的眼睫似有水光，微微顫動著。

「殿下。」

「哼。」

「多謝你。」蒼淡淡地笑了一聲，「未來的日子，想讓蒼陪你做什麼？」

武神端詳了一會兒道者沉靜似水的面龐，很認真地說：「吾想和你成親。」

「噗——」蒼噗嗤一聲笑了出來。

「笑什麼笑！不許笑！」金藍異瞳悶悶不樂地瞪了道者一眼，「這好笑嗎！」

「嗯嗯」蒼清了清嗓，隨即正色，「好。」

⋯⋯想必又是話本上看來的。

<br/>

「聽說要八抬大轎，洞房花燭，」武神饒有趣味地描繪著，金藍異瞳如孩童般閃著興奮的光，「就讓吾的魔兵，給你抬轎。」

「別，別⋯⋯」蒼連忙打斷，「樸素些好。依蒼看來，倒不如尋一處僻靜的山林，幽山溪澗，鳥獸蟲魚，與自然為伴豈不更是快哉。」見武神眉頭微蹙，蒼不急不慢地補充，「再說，你的私事，有必要讓其他人知曉嗎。」

「說的也是。」

蒼松了一口氣，「那何時出發？」

武神遲疑著問：「明日？」

蒼淡然應允，「好，就明日⋯⋯」

<br/>

燈影下氣氛終於漸漸和緩，武神攬著道者在榻上，在榻上面對面躺下，手指撥弄著道者的長髮，「把你的過去，講給吾聽。」

「嗯。那就從吾年少時說起吧⋯⋯」

金藍異瞳慵懶地瞇起來，「嗯。吾都要聽。」

<br/>

伴著搖曳的燭火忽明忽暗，道者將往事娓娓道來。

「吾長大的地方叫作封雲山，位於道境。封雲山三面環海，另一面由一座小鎮接連廣袤的平原。這座小鎮叫月華之鄉，那裡有一株古老月華樹⋯⋯吾年少時，經常會從月華之鄉繞路去海邊，聽那裡的濤聲⋯⋯」

說到這裡，蒼突然猛地一頓。

「怎麼了？」金藍異瞳緩緩張開，慵懶地打量一眼道者，「蒼，你似乎想起了什麼？」

道者的面龐卻很平靜。「哈，只是想起了困擾吾許久的一個疑問。不過，那是很久以後的事了，今日暫且不提。」

「哼，隨便你⋯⋯」

「道境，封雲山，吾在那裡度過了大半生的時光⋯⋯」

「封雲山，很美嗎。」武神瞇著眼睛，很輕地問，漫不經心地撥弄著道者柔軟優美的淺色長髮。

「嗯，很美。」蒼閉上眼睛，隱去了眼中的水光，「吾會永遠懷念那段日子。」

「道境，封雲山。」武神突然重複了一遍。

「嗯？」

「蒼住過的地方⋯⋯吾，記住了。」

<br/>

> ##### 小剧场I
>
>（初始版魔界）
>
>魔兵A：不好了，不好了，魔皇拋下我們，和他的皇后叛逃了！
>
>魔兵B：笨蛋，那哪叫叛逃！那叫私奔！
>
>魔兵C：嗚嗚嗚，魔皇和皇后私奔了，不要我們了⋯⋯
>
>魔兵D：那不是挺好！要不然打了敗仗他還要擊碎咱們的腦殼！散了吧散了吧，告老還鄉囉～
>
>（眾魔兵溜走.....）
{: .block-tip}

> ##### 小剧场II
>
>蒼：你的魔界就這樣扔了？
>
>武神貓貓：哼，隨時可以再造的東西。（趕緊把蔥花寶貝親親抱抱舉高高）蒼說不要魔界，那就扔了吧
>
>魔龍：嗚嗚，咕咕咕，（好餓，肚子咕咕），嗚嗚嗚
{: .block-tip}

> ##### 小剧场III
>
>（貓咪絕育處）-->> 是說這貓絕育好幾天了怎麼還在啊XDDD
>
>棄貓：（怒捶小電視）可惡！你有沒有事業心！辣麼大的魔界你說不要就不要！戀愛腦！丟臉！呸呸呸！晦氣！
>
>某藍：（插嘴）喂，棄總，你的魔界也完蛋了，就不要說武神貓貓啦（話說你就是因為吃醋才對武神貓貓惡語相向吧）
>
>棄貓：（貓爪亂拍）閉嘴！放肆！退下！
{: .block-tip}
