---
title: 十戒
layout: post
lang: zh-Hant
category: ten
depth: 2
date: 2011-03-21 00:00:28
author: 文心玲
---

### 回二十八
越過蔚藍的海水，飛過島嶼與島嶼之間的距離，類似的爆炸聲亦在蒼身後響起。
他回頭一看，棄天帝的黑色ＢＭＷ雙人座轎車已成火海。

『他們的動作真快，看來阿棄順利完成他想做的事了。』

太陽神的語氣頗是欣慰，接着他猛踩油門。 

蒼只有雙手被膠帶綁縛，他其實還有反抗的自由，但他也想知道棄天帝的行蹤，以及在棄天帝兄弟身邊一連串的謎團的答案，對於棄天帝的信息他知道的太少，他不希望自己對棄天帝的背景一無所知。 

棄天帝並不知道，蒼從一開始只被告知該如何拐棄天帝到指定地方，蒼只知道棄天帝以演員掩蓋黑道幫派老大的身份，或者更精準的說，蒼只被命令何時何地該做什麼事，至於他不需要知道的事情，蒼並沒有追問。 

蒼一直以來的人生都是這樣渡過，他被動的依照指示行動，在那個人容許範圍之內蒼可以有交朋友與玩樂的自由，當蒼的小學老師出現在他面前表達要簽下他的那一刻，蒼以爲他可以脫離被操控的人生，但其實一切仍在那個人的掌握之中，蒼進入演藝圈更容易辦一些白道不能出面的事情，演藝圈中五光十色與聲色犬馬無一不缺，唯一缺的只是真心。 

他的手雖然被膠帶綁住，但一舉起手臂仍能看到套在他指間的黃金翅膀戒指，就像看到棄天帝的真心般令人感動。

蒼從後車窗看到棄天帝的車子被火舌吞噬，幾個人影正小心翼翼地在車子四周地上找尋，一個人影冒險往車子噴滅火器，但看來不像是想救人，反而像是想確認車內是否還有活人，蒼轉頭坐回後座，他不想繼續看下去。 

在這個社會上，見不得光的 黑 道 底層遠比一般人想象更危險，那些名不見經傳的熱血年輕人總是被哄騙着自告奮勇地被犧牲，然而真正得利的卻不會是那些人，利益最終只會歸於在臺面上被洗白成 集 團 董 事 長 的 黑 道 老 大 與 掛 勾 黑 道 的政 客 們。

蒼不必猜想都知道後面會發生什麼事，也因此他不願意去看，他不想看見年輕生命無故地犧牲。

『蒼，你不好奇發生什麼事嗎？』伏嬰師轉頭看着他。

『我很好奇，但是知道又能如何？』蒼輕聲嘆氣。 

『知道了能改變那些人將死的命運嗎？還是知道了你們會放我跟翠山行走？』 

『不能。』伏嬰師很誠實地回答。 

『既然你都猜到了，更不可能放你走。』

話才剛說完，車後的遠處便傳來爆炸聲，蒼不必回頭也知道發生了什麼事，車上另一個炸彈被引爆了。

雖然他們的車子已離爆炸處兩公里遠，但仍能感覺到強大的爆裂引起的地面震動。 
翠山行的嘴被膠帶封住，他睜大了眼看得出被驚嚇到，蒼挪動身子靠近翠山行，想以肢體接觸讓他安心。

『別怕，沒事的。』他輕聲哄着，像是哄小孩睡覺一般的溫柔。 

翠山行看着蒼的紫眸，信任地點頭。 

蒼想伸手摸翠山行的頭髮安撫他，但因是手被綁住無法行動而作罷。 

太陽神從後照鏡中將蒼的行動看得一清二楚，他的薄脣勾起一個似笑非笑的弧度。 

『對於我們的事情，你們瞭解多少？』

很明顯太陽神話裏所說的『你們』不是指蒼和翠山行，而是蒼的家世背景。

『我不清楚。』 

『是不清楚還是不能說？你的性命掌握在我手裏，勸你別妄想當忠肝義膽的死士，我絕對有辦法讓你招認出來，只不過到時候你不會好受就是了，哈。』 

伏嬰師看情況不對趕緊插話。

『蒼是無辜的，他一無所知。』開玩笑，讓你動了蒼，我會被棄天帝罵死。

『是嗎？』太陽神看着伏嬰師的眼睛，他微笑。

『小伏嬰，你知不知道你一向不擅長說謊，這世界上只有朱武一個人會被你的謊話耍得團團轉，但你若要騙過我，再修練個十年吧。』

伏嬰師氣得不想與太陽神再說一句話。

蒼沉默着看窗外的景物飛逝，路樹與路樹因爲車速太快而連成一片綠色的面，綠色的面永遠不會與道路上接連不斷的線交會，面與線之間雖然在同一個時空，本質卻存在着無法橫越的分歧，除非構成面的路樹因強風吹拂而傾倒，面與線之間才有機會交會成一個點。 

他與棄天帝也生活在同一個城市，甚至作一樣的工作，但除去了表象之下，他們卻像是不同世界的人，如果他一直不想去觸碰，那麼他們永遠不會有交集點。

他想伸手去觸碰笑臉表象之下的棄天帝，希望不會因此而被黑暗淹沒。

『我並不能稱爲無辜，我曾經順從父親的命令帶棄天帝進入預設好的局，只不過我臨陣退縮了。』

車上陷入一陣靜默，靜得能聽到蒼自己的呼吸聲，他緩道：

『我只知道棄天帝曾殺害很多人，其中包括許多接受萬聖嚴基金會關懷的孤兒們的父母，我同時也知道這些人都曾參與過 幫 派 火 拼，或者甚至曾是殺手。也許你們會認爲江湖中人本該殺人人殺，殺人對你們而言不算什麼大事，但我卻覺得是因爲有 黑 道 幫派才使得這些人墮落，那些所謂的江湖情義根本只是騙局，我相當厭惡像你們一樣的 黑 道 領袖之子。所以我曾經逃避棄天帝的感情，以爲拒絕他就能對他狠心，結果卻是我自己的心淪落得更爲徹底，因爲太過防備反倒陷落，這也許就是我的報應。』 

蒼自嘲地笑了，但聽衆卻無法笑得出來，他又接續說道：

『當我發現我自己的心情之後，我就陷入了痛苦的自我掙扎，我故意讓棄天帝跟蹤我到銀行，讓他看着我把他爲我而策劃得來的光盤放入保管箱，我甚至在他面前晃了晃保管箱鑰匙。我以爲他會生氣，我以爲他知道我利用他後會離去，但他並沒有，即使我告訴他我將有一日會全盲，他依然不在意，我被迫得甩開他的手。』 

蒼緩緩說出的每一個字都像在剖開自己的心，讓心情血淋淋地攤在陽光下，等着流血然後幹凅癒合。 

蒼的表情並沒有太大的起伏，但翠山行卻已淚流滿面。

他知道蒼不會哭，所以他替蒼哭。

伏嬰師貼心地遞上面紙，然後他想到翠山行雙手被綁起來，他只得動手幫翠山行擦去眼淚。

『我知道你爲阿棄也付出了很多，你身爲 政 治 家 的私生子，你能得到與阿棄在一起的自由嗎？在你戴上阿棄的戒指之前，你付出了什麼代價？』 

蒼平淡地說道：

『我付出我能付出的所有，但你不需要知道代價爲何。我該說的都已說完，該輪到你告訴我棄天帝的下落。』 

太陽神眨眨眼，玩笑地回道： 

『不說又能怎樣？你會咬死我嗎？呵呵呵。我開玩笑而已，別太認真。說着說着我家就到了，請隨意參觀。』 

他停車後下車開門後徑自走了，伏嬰師先爲翠山行與蒼松綁，帶他們下車。

『請進。』伏嬰師鞠躬說道。

蒼牽着翠山行的手走進豪宅門口，他知道他正踏入棄天帝的世界，他也知道踏入後不可能再回頭，但他並不後悔。 

人的一生中總該隨着自己的心放縱一次，如果棄天帝會是他人生中最高的浪頭，他願意隨着大浪起飛，而後乘着浪頭歸於最終平靜的海洋。

棄天帝踏上臺灣的土地，他擡頭看着天空，天際泛着濛濛白。

他以自動系統繳清停車費，而後他發動太陽神的雪白跑車，快速離開桃園機場。 

無雲的夜空能引發人的浪漫情懷，棄天帝忽然生起一股想開車到蒼家裏約他看日出的念頭。

他看了看手錶，臺北標準時間凌晨四點三十分，蒼應該睡得深沉，看來看日出是不可能了。

遺憾地搖頭，棄天帝想起今日還有幾場戲要趕拍，得把握時間熟讀劇本，不然若因爲他的日本行而連連ＮＧ，蒼可能會與其它演員聯合起來揍他一頓，哈哈。

棄天帝握着方向盤，他的心中滿是蒼的容貌，他很期待到片場後蒼見到他的表情，會不會彆扭地別過頭遮掩臉頰的紅暈呢？

還是蒼會戴着戒指緩步向他走來？

此刻的棄天帝恨不得下一秒就能看見蒼的身影，即使他不理會他也好，他也有自信能重新贏得他的笑容。 

棄天帝知道他的想法很傻，但爲愛情傻一回也未嘗不可。

爲了蒼，再傻也值得。

(雖然我很想打全文完,但還是未完待續)


