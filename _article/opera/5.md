---
author: 西陵歌者
title: 戲
lang: zh-Hant
date: 2010-06-22 00:00:05
layout: post
depth: 2
category: opera
---

### 第五場 棄家公館Ｃ



依舊是舊曆的八月十三，中午時分。

戒神老者再次敲開了安安靜靜的房間的大門，“蒼先生，方才Ｊ城商會會長和副會長前來拜訪老爺，此時三位已經起身去泰豐樓了，廚子做好了午餐，請您去餐廳吧。”

“啊……麻煩了。”蒼略微有些失望，不過，卻也不覺得有什麼了，跟着戒神老者走到一樓的小餐廳去。關照之後，戒神老者也去後面廚房和傭人一起吃飯，蒼獨自坐在桌邊，看着四個精緻炒菜，旁邊瓦罐之內還有煨好的骨湯，伸手抓起一個純白麪的饅頭，掰了一半——棄公館廚子手藝不凡，食材又是上好，即便是家常菜餚也甚是可口，倒叫蒼覺得有些過意不去了。才慢慢吃了小半個饅頭，便聽見門外又有腳步聲了。

“蒼先生，吃完了麼？”戒神老者走了進來，後面跟着的竟是補劍缺，“老爺請您去泰豐樓了。”

“是。”補劍缺略一點頭，“長官與蕭會長、冷副會長正在等着，請蒼先生現在上車吧。”

“嗯？好，有勞了。”有些尷尬地放下手中饅頭和筷子，此時戒神老者已經遞過了一塊溫熱的毛巾，蒼擦了擦手，再整整衣襟，便跟着補劍缺走出大門上車了。

<br/>

泰豐樓在商埠區，距離棄公館說不上近，然而坐着汽車也只用十多分鐘便到了。這酒樓乃是個兩進的院落，前面是個三層高樓，正在午時，熱鬧得緊，到了後院包廂倒反而幽靜了。剛剛提起前襟跨過一道門檻，來到後院之側越發別緻清雅的一座小院，便聽得內中Ｊ城鎮守使洪亮地笑聲了家公館



“您稍等，我進去通報一聲。”

進了院子，補劍缺搶在蒼的前面，阻住了他本打算再向前一步便站住地腳步。

蒼松手撒開了長衫的下襬，便立在院門口的那顆石榴樹下靜等。屋內窗戶上已經換了保暖的洋玻璃，雖然應比棉紙透明，但是此時日光太強，卻恰恰反射光芒，什麼也看不清。只過了片刻，見幾名警衛走了出來，將這正屋的四門大敞，露出內中三名在座的Ｊ城名流。

端坐在主位，棄天帝等到房門打開，屋內亮起之後，才擡眼看向院中。正午陽光之下，立在院中的蒼，玉像一般地精緻倒叫他覺得訝異，不過，總也不至於因此失神失態，便那樣坐着不動，輕輕擡了擡手。

原本覺得已經有些熟悉的棄天帝，此時竟如一個陌生人一般和屋內兩名衣冠楚楚的中年士紳一起，饒有興趣地審視自己，蒼竟是如同被背棄了一般，情不自禁地顫抖了一下，然而，沉靜心思，略微反省，還是鼓起勇氣緩步上前，立在院中，雖然不願，卻也只有正視對方，微微鞠躬，輕聲問說：“棄長官……要我來此，乃是爲了何事？”

“哦？”聽慣了“長官召見，有何吩咐”這樣的話語，棄天帝對蒼的這種有些不諳事故然而又不失禮數的言辭覺得有幾分新鮮，嘴角微微一翹，便與對方對視，卻不回答。

“咳，”坐在上首客位的蕭振嶽微微轉身說：“蕭某人久仰蒼老闆與赭老闆大名，方才閒聊之時談起，不想蒼老闆也在棄長官處做客，便想請個近水樓臺之便……”這句話倒是不假，席上三人的兒子年紀相仿又正當青年要緊，閒談之中，自然是不可或缺的話題，而蕭家冷家兩位公子，倒是都對封雲社那兩位梨園新秀讚譽有加。

“嗯？”微微愣了一愣，又看着正對面屋內的棄天帝，“棄長官是要蒼……”

“唱《思凡》吧。”不理對方問話，棄天帝彷彿理所當然微笑說，隨後指了指手邊自己的茶杯，示意補劍缺給蒼端了去潤潤嗓子。

“……如此，獻醜了。”接過已經溫涼的半碗殘茶，略微潤潤嘴脣，卻見補劍缺已經從屋內青花瓷瓶中將一隻雞毛撣子抽了出來，遞給自己權作拂塵道具；同時，又有夥計幫忙，從門口搬了把椅子放在院內。

……

蒼輕輕閉目，深吸口氣，再睜開之時，只當腳下乃是雙儀舞臺的臺板，而面前所坐者，皆是素不相識地看客了。

【昔日有個目蓮僧，救母親臨地獄門。借問靈山多少路，有十萬八千有餘零……】

側身斜坐，心中鬱郁之情，便似隨着這戲文宣泄一般，一發不可收拾。同時，一側遊廊之上，傳菜的夥計來來往往川流不息，屋內已經開宴了。

……

【冤家，怎能夠成就了姻緣，死在閻王殿前由他。把那碾來舂，鋸來解，把磨來挨，放在油鍋裏去炸，啊呀，由他！則見那活人受罪，哪曾見死鬼帶枷？啊呀，由他，火燒眉毛且顧眼下。】

……

“哈哈，妙啊，妙啊！”

蒼收聲之際，蕭振嶽與冷霜城同時撫掌喝彩。等到聲音歇了，棄天帝看看適才還活潑如同一隻小兔一樣地蒼靜靜立在院內帶着爽然若失地迷茫表情；便慢慢吩咐，“補劍缺，給蒼老闆加副筷子。”

“棄長官與兩位會長在座，焉有蒼的位置，如無其他事情，蒼告退了。”低頭慢語，卻是再也不和此席主人對視了。

“坐。”棄天帝臉上那若有若無的笑容不減，左手放在桌上，看着在院內依舊不動地人，又提高了聲調，“坐！”

“……”仍不回話，甚至幾乎是本能的微微把頭偏了偏。

蕭振嶽覺得氣氛不對，向對面的冷霜城使個眼色。

“哎呀，哈哈哈，”冷霜城乾笑起身，來到院內，說：“蒼老闆莫要拘束，能夠結識蒼老闆這樣的妙人，我們才是榮幸啊。”說着一拉蒼的胳膊，將他拖入屋內。

冷霜城年紀做自己父親有餘，蒼心中雖然不願，卻也無法再執拗下去，走入屋內，蕭振嶽已將原本放在末位的椅子向自己這邊拉了拉，笑說：“正是，蒼老闆，請坐老朽身邊來吧，犬子對您讚不絕口，老朽也想同您多親近親近啊。”

……

宴罷，棄天帝起身走出，補劍缺不動聲色靠近了蒼的身邊，“蒼老闆，請吧。”

“何處去？”

“跟我走。”已經走到門口的棄天帝，聽到了這聲冷冷問話，竟是停步轉身，淡淡地理所當然地回答。

此時飯店夥計搶着跑過去，將Ｊ城鎮守使的專車後門打開，棄天帝走過去立在門邊，卻不進入，反而是看着跨出門檻的蒼……

<br/>

坐在慢慢在午後清淨的街道上行駛地汽車後座右側，蒼默不作聲低着頭，看着放在腿上握在一起地自己的手。儘管目不斜視，卻也知道身邊那人一直在饒有興趣地看着自己。突然，一隻手碰到了額頭，微微挑起蓋着額角的幾縷亂髮。

程度之外的震驚，蒼毫不猶豫地甩頭躲開，微微側過臉，帶着幾分驚異和更多地不滿看着正盯着自己地棄天帝，隨後，下頜竟被霸道地抓住了，臉被迫揚了起來，另一隻手推開蓋在額前的幾縷髮梢，異色的眸子靠近，瞳子中出現了那道淺淺疤痕。

“哈，還以爲看錯了，原來真是個疤。”儘管緊貼着髮際線，儘管顏色已經很淡，然而打量良久，仍是可以察覺，棄天帝右手食指輕輕放在那傷疤上仔細摸了摸，“這種程度，倒是不影響扮戲……怎樣弄的，戲子不應該好好保護自己的臉面麼？”

“……拜棄長官所賜。”捏着下顎的手如此有力，雖不是刻意欺凌，卻讓蒼覺得窒息了，艱難地吐出這幾個字，一直沒什麼血色的臉上，已經透出了不正常的紅暈。

“嗯？”手上情不自禁的加了力，思忖之中，發現對方露出了痛苦地神情，才終於鬆了手，看着蒼壓抑着什麼似的，一面微微喘息，一面僵硬地整理衣領，雖然很想問問這句話什麼意思，但是卻不知是什麼情緒阻止自己問出口，緩緩把身體轉正，透過正面的車窗，看着在午後也變得有些冷清的Ｊ城城最繁華的大街，沉默了片刻，沉聲問說：“你和朱武，什麼關係？”

……

閉了眼，又看見一滴一滴鮮紅的血落在身下海河的薄冰上，慢慢滲入一條條讓自己戰戰兢兢地細縫中，這熱度，似乎是可以輕易融化初冬的薄冰，然而卻溫暖不了順着手掌和足尖傳來地刺骨的寒意。不動，彷彿就要凍僵了再也無法動彈，彷彿下一刻，這承載這自己微不足道的體重的冰面便要碎裂或者那些端着槍在身後的河岸上監視的大兵便要開始掃射，然而，仍需放慢爬行地速度，“蒼，蒼啊，快過來！”迷迷茫茫地擡頭，看見師父和師兄弟都已安然站在了對岸，很少露出笑容的自己，在那樣一個慘淡到窒息的清晨，竟是真地笑了……

很不習慣等不到回答的靜默，棄天帝微微側頭，看着身邊的孩子臉上露出悲喜絕人寰地神色，竟被莫名震懾了一下，不再吭聲了，直到汽車停在Ｊ城最大的綢緞莊——瑞蚨祥鴻記那西洋風的店面門口時，才輕輕碰了碰已不知何時將頭靠在車窗上的蒼，說：“下車。”

“啊？”睜眼，卻發現停車的地方竟不是麟趾巷也不是南崗子，蒼一時恍惚了。

“下車。”

瑞蚨祥在Ｊ城的這家店，聽說是用修建黃河鐵橋的剩料修建，在Ｊ城衆多店鋪之中，可算得是頭籌了。門前的樣子和室內的裝飾，也是相當西式的，特別是門口兩側，那兩個頭頂帶着渦卷的壁柱，倒是顯得典雅了。此時綢緞莊的掌櫃的早就等在門口，直接將兩人請進了二樓寬大的貴客室。

脖子上搭着皮尺的裁縫和學徒誠惶誠恐地進來躬身，棄天帝坐在那西洋的大沙發上，指了指立在茶色玻璃茶几前似乎還有些迷茫的蒼，“給他做。”隨後，端起面前的茶盅，慢慢品着地同時看着在天窗投射的光芒之下，兩名裁縫圍着蒼量尺寸。

“棄長官？”有點疑惑地看着沙發上的人，“蒼不需要……”

側頭打量竟是比裁縫和立在一邊賠笑說話地綢緞莊掌櫃還要緊張地蒼，棄天帝微微笑笑，也不理會，轉頭說：“有什麼新花樣，適合做套衫。”

“是，是是是！”掌櫃趕緊吩咐門口的夥計，“去把天字櫃上新來的花樣挑上好的抱上來！”

……

一面應付裁縫的擺弄，一面時不時看着已經站起身，在堆滿了沙發和茶几前的波斯地毯的布匹中認真斟酌的棄天帝，蒼只覺得這個人，是自己永遠捉摸不透的存在。

拎起一匹，示意夥計將之披在蒼的身上看看顏色和花紋，棄天帝的認真程度，叫周圍的夥計也有些看不透這位青年人究竟是什麼身份了。

“敢問軍爺……”掌櫃慢慢退在一邊，悄悄地問補劍缺，“這位小公子，是棄長官的什麼人啊？”

“……是……朱武少爺的朋友。”補劍缺有點爲難，含糊回答。

“哦？”掌櫃再次看了看眉頭微鎖地蒼，突然一驚，湊向補劍缺的耳邊說：“是說，我們這裏也有幾名女裁縫的，專門給太太小姐們做衣服的，要不要叫她們上來，替這位量身？”

“嗯？”

“掌櫃，”此時，挑定了長衫與外面馬甲的料子，棄天帝慢慢轉身，“做洋服的料子也拿上來，色淺的，這個季節的。”

“是是是，是是是，最近訂的一批外國貨剛剛到。去！快給長官拿上來！”

……

“報告長官，長衫和馬甲，約莫明日早晨便可做好，洋服是要再等個一兩日的，中間若有時間，最好再來試試樣子尺寸。”

“甚好。”

從瑞蚨祥出來，已是下午四點多了，棄天帝點點頭，慢慢走下臺階，吩咐正替蒼打開車門的補劍缺：“達通遠鞋店。”

……

“明日國貨商場開張剪綵，你同我一起去。”坐在車內，駛在黃葉飄落的街道上，棄天帝慢慢說。

蒼身體微微一震，沉默了片刻，終於一字一字地回答：“棄長官，蒼見識短淺，身份卑微，出席那樣的場合，並不適宜。“

“不願去？”眉梢抖了抖，沉聲問。

“……是。”鬆開了咬着地下脣，一面說，一面彷彿唯恐產生誤會一樣，點了一下頭。

“我不喜歡任性的青年人。”

“……如果這是相救赭師哥的條件，蒼願去……”

“……哈。”轉回頭笑了一聲，對着前面的補劍缺說：“回公館。”

<br/>

“老爺，東宮少爺來了，已在客廳等了您一會兒了。”

走入大門，戒神老者出來迎接。

“請去書房……補劍缺，你去。”棄天帝吩咐完畢，已走上客廳的樓梯，回房換衣。

戒神老者心領神會，便抽身陪着蒼回到二樓的臥室去，便告退出去了。

隨着門關上的一聲輕響，蒼便仰天倒在床上，渾身說不出地疲累，心中亦是陣陣抽痛，擡手撫上額角的傷痕，那人那完全事不關己的態度，讓人憤怒——然而又能如何，想到懷着這樣地憤怒竟還要去他席前唱曲助興，竟連自己也厭惡起來……迷迷糊糊，感覺什麼跳上了床，就睡在自己身邊，熱乎乎的發出輕微地呼嚕聲。戒神老者心領神會，便抽身陪着蒼回到二樓的臥室去，便告退出去了。蒼翻了個身，彷彿尋求溫暖一般，微微屈身，將之圍在懷中。

“哎呀……定是蔥花淘氣。”從不知什麼時候被撥開的門縫中看見，雙腿垂在地上，蜷縮在床邊懷抱蔥花睡覺地蒼，立刻心疼地跑進來，輕輕拍拍蒼的肩頭，“蒼先生啊，累了便躺好了睡吧，這樣還不着涼啊。”

“嗯……”心中略微明白，然而卻彷彿被鬼壓床一般，只能發出如同懷中貓咪一樣輕微地哼聲，任憑戒神老者幫自己脫了鞋子，將雙腿放上床，再拉開被子蓋在身上，一陣溫暖，手放在貓咪身上，睡得更沉了。

<br/>

“蒼先生啊，醒醒……”

終於被叫起來的時候，天早就黑了，床頭點亮的燈還有點刺眼，蒼微微睜眼，見到戒神老者端着杯水立在床邊，壓着嗓子輕輕叫了聲：“戒老……”

“蒼先生啊，下樓去吃點東西再睡吧。”

“啊……什麼時候了？”

“已是九點多了……”——晚飯之前曾之前過來了一次，見蒼睡得沉，便也沒有叫醒，此時天實在太晚，又見他漸漸睡不安穩，知道是渴了餓了，戒神老者便又走進來叫醒了。

“嗯……”嗓子幹得彷彿要冒出火來，接過杯子，竟連聲“多謝”也顧不得說，迫不及待地一飲而盡，才從脣間品出蜜水淡淡的甜味來，走去後面的房間略微洗漱，便跟着戒神老者下樓去中午吃飯的那間餐廳了。

<br/>

“這是……？”

“這是紅樓金店的大小姐親手烤的西式奶油蛋糕，下午東宮少爺專門送來的，不過老爺不喜歡吃甜食，嚐了一口吩咐留給您了。”

有些好奇的看着桌上鋪滿白色軟膏點綴着紅色櫻桃的點心，蒼慢慢轉到對面的椅子前面坐下，果然看見切去的一角缺口內中，露出發麪團的肌理。此時戒神老者已經替蒼切了一塊下來。

“好甜……”小心翼翼用餐刀切下一塊放進嘴裏，蒼的眼睛便眯了起來，這時一股蔥花醬油湯的香氣傳了過來，竟是與這蛋糕的香氣意外的搭配。

“怕您吃不慣，剛才又給您做了點麪條湯，配合着吃吧。”

“多謝……戒老，您可知，我師兄他現下……”

“這個……老兒實在不知。”

“……哦。”默默低頭，看着面前盛在細緻如玉的西洋款式的白瓷碗內打着蛋花的麪湯，“……不知道，塵音、雲染他們，此時……”

……

奶油蛋糕有點膩，還是喝了那碗麪湯舒服，回了自己房間，準備聽從戒神老者的建議，再洗個澡了。等待浴缸放水的功夫，見到窗口外面，隔着窗簾，有些燈光透進來，此時倒是不困，慢慢走過去，想拉開窗簾看看燈光何來，誰料，露出的陽臺上，卻恰恰一個人影走過……

“啊……”

從辦公室走出，從房間外面的觀禮臺上去三樓自己臥室的棄天帝被突然透出的燈光攔住了腳步，不動聲色轉頭，似乎是上下打量了一下還算是剛剛起身衣衫有些凌亂的蒼和蹲在他腳邊不知道從何處鑽出來，歪頭看着自己的貓咪蔥花，便又轉過身，繼續登上室外的鑄鐵臺階，從那裏上去三層的露臺，回到自己的臥室去了。

愣在窗口，聽着對方的腳步聲慢慢遠去，才猛地把窗簾拉上，轉身長出了口氣，想不到外面會有人，蒼真是被嚇了一跳，映入眼簾的那在燈光下有些詭異的異色雙眸的面容，竟還停留在眼前一般，心臟跳動得格外厲害，雙腿反而無力地想要坐倒了。此時，盥洗室內的水聲有異，猛然一驚，慌忙跑過去，關了龍頭……

<br/>

<br/>

翌日，舊曆八月十四又是一個晴天。

“……不差。”

放下手中的晨報，看看立在面前的蒼，棄天帝眼中露出一絲驚豔，心裏很滿意自己選中的這套淡淡的雪青色：雖是很美麗的顏色，但是卻不華麗張揚，溫柔中帶着讓人心安詳的平靜和雋秀。

又是這種純觀賞地眼神，蒼微微側頭，馬甲領口滾邊的白兔毛拂過面頰，溫暖得很——這不是自己應該穿在身上的衣服，然而早晨的時候一個疏神，戒神老者已經趁着如廁的時間將自己的舊衣抱去下面洗了。

“老爺，方才來送衣服的夥計說，洋裝大約下午便成形了，若是有時間，請蒼先生過去試試，看看衣襟袖子長短，定型之前可再改改。”

“下午來吧，你看着辦。”棄天帝隨手將報紙疊了疊，放在身邊，隨後擡頭看看將要說話的蒼，一伸手，“坐。”此時，僕人已將今日的早餐送上，棄天帝家規，早餐乃是一日西式，一日中式，今天碗中黃澄澄的小米粥還有些燙，棄天帝慢慢用手中的青花調羹攪拌着，同時似乎還有些意猶未盡地打量坐在對面的蒼，突然問了句：“沒睡好？”

確然是沒有睡好，本來下午睡了一覺，晚上就不容易入眠，何況雖然後來找到了陽臺門的插銷，認真地拴好，卻是在床上翻來覆去，不知用後背還是正面對着那巨大的窗口好了，直到後半夜，似乎是一直睡在床下的蔥花覺得冷了，老實不客氣地跳上床在腳邊臥着，才覺得稍微安心，慢慢地忍到天亮，至於有無睡着，是實在不記得了——只是這些原因，實在是不足爲外人道，況且是在此人面前，蒼張了張嘴，道：“棄長官，未知赭師哥現在如何？”

“我無需向你報備。”說完之後，已將瓷勺送入口中，待嘴裏食物嚥下，才擡起頭，看着蒼的眼睛，似乎是不懷好意地笑說：“準備好需付的代價即可。”

慢慢將頭低下，蒼亦沉默了。

<br/>

今日中午，棄天帝需出席國貨商場的揭幕，晚上，紅樓金店的大小姐曌雲裳按照慣例在家中舉辦中秋宴，邀請Ｊ城名流，朱武不在，棄天帝命補劍缺去到皇華館將黥武接來，倒叫這可愛的“學生弟”成了當晚在場的太太小姐們玩笑戲弄的對象了。







