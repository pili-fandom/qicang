---
author: 西陵歌者
title: 戲
lang: zh-Hant
date: 2010-06-22 00:00:16
layout: post
depth: 2
category: opera
---

### 第十六場 棄家花園Ｂ



手臂終於拆了夾板，感覺實在是輕鬆了很多——棄天帝再次微微活動了一下似乎已經僵硬的手腕，目送從麟趾巷追到這裏來給自己複查地敬業地德國大夫，心裏有了不覺察地盤算。

“長官召見，有何訓教？”斷風塵是從自己家裏被叫來的，懷着逃班濟私被上司抓包的心緒，抹下了冷汗，才走入房門大敞的Ｊ城鎮守使辦公一室，躬身問道。

“嗯……開車送我回家。”補劍缺大抵是回不來了，更重要的是：棄天帝也一時記不起來，片刻之前猶豫半天終於下定決心將正準備婚禮的斷風塵叫來是爲了什麼事情。

“呃，是……”

<br/>

“老爺，您回來了！”

下車見戒神老者正在門口將一群長衫瓜帽之人領入，四五個夥計打扮的人背上都是幾十匹用油紙包着的布匹，顯然是瑞蚨祥的裁縫應招而來。戒神老者讓這掌櫃夥計大大小小七八個人在客廳一角等着，正要上樓去叫三層地各位少爺先生，卻聽見車喇叭聲響，從樓梯旁的窗口見警察廳的汽車已經開到門口，斷廳長親自開門，從車上下來的是午後突然如同負氣出走一般說要去鎮守使公署辦公的老爺。

走進大廳，將手套大衣交給僕人收着。斷風塵在身後告退時，棄天帝突然微微側轉半個身子，問說：“婚禮，柳生君也會來吧？”

“正是，柳生先生與夫人已經發來了電報，明日一早便到了。”還沒有正式成親，斷風塵這句二姐和二姐夫還是叫不出來的。

“請他合適的時間邀來公館一晤。”

“是……請問長官什麼時候方便？”

“……午後吧。”說這句話地時候，Ｊ城鎮守使舉步走上直通三層的樓梯。

“老爺，大夫替蒼先生檢查過了。”戒神老者追在大步上樓的棄天帝身後，有些緊張地稟告。

“結論。”

“說是這幾日，如果蒼先生感覺好，可以先短時間練練功，做些恢復性的活動；開口唱戲，還要再等幾天。能不能長時間登臺，七天後再來複查。”檢查時候，戒神老者就在旁邊，看着聽到那德國醫生的中國助手轉述的醫囑，蒼的眼神漸漸暗淡，他便記得越發用心了。

“嗯。”對結果，棄天帝並不意外——那面色蒼白的年輕人的狀況，除了本人之外，對其他人而言都是一看便知，不過竟是難得自言自語一般，多問了一句：“……心緒更糟糕了吧？”

“啊？嗯，蒼先生……也不是能輕易肯……”

微微擡手，棄天帝表示明瞭，開始繼續聞訊下一位家人，“朱武？”

“少爺剛剛進門不久，現在應該在房間換衣；倒是黥武少爺，一下午都不見有什麼動靜。”回答兩位少爺的動向，戒神老者倒是熟練得很，不等寡言的老爺繼續逐個聞訊，便直接說了。

“叫裁縫去三層大房間，先給他倆量身。”吩咐完畢時，棄天帝已經轉過三層樓梯間，徑直推開了起居室的門。

……

蒼已經穿上了那套淺色的洋裝。

棄天帝走後，朱武陪着他頗坐了一段時間。聽他閃爍地言辭，分明是想要寬慰別人，然而由不得是自己也糾結起來，蒼幾乎就要忍不住將那戲幕後之事和盤托出了。然而約定在先，亦覺得實在沒什麼立場真的牽涉進這對父子之間微妙地對峙，若說難受……蒼突然想起：棄天帝每次靠近自己，指間淡淡的菸斗殘餘的氣息卻似乎有越發濃郁地趨勢了。

……

“蒼，我真的……真的對不起，但是……我……真地不想……雖然我知道很多人不喜歡父親，但是那是因爲他們不認識他，不懂得他……但我……他是我父親，……我不想你也……更不想我也……”

“……我明白。”這話出口，蒼倒真的不是敷衍了。

“蒼……如果……父親願意負責，你是不是可以……”

“朱武少爺……”不知該怎麼說，蒼唯有淡淡地苦笑，對朱武其實一直懷着一份因爲欺瞞而生地歉意，“您和棄長官之間的感情請如往日一樣，不要顧慮我；蒼和棄長官……究竟如何，也是不需朱武少爺這樣煩心的。”

“蒼，你別這樣……那我們父子豈不是成了自私的……”

“……不是這樣的。”

<br/>

……如果不是這個時候，醫生到來解圍，蒼還真不知要如何將這對話繼續下去。唯獨之後的時間，對於自己而言亦非解脫，而是同樣地壓抑。

“聽到醫生的話了？”那個人上樓的腳步聲，蒼輕易就能聽出，是以低沉地聲音在門口赫然響起，並沒有如以前一樣受驚，只是突然意識到了：這公館內中的一切，在此人心中皆爲自己所有，因此從來沒有什麼顧忌，或是行動要預先通報的必要——這是……家主的自覺麼？

“是。”

“……你不會覺得是我買通了醫生吧？”突然想也不想冷笑了一聲，說話者與聽話者都沒意識到，這是爲了中午那句問話的報復。

“棄長官不允許蒼離開，一句話便足夠了。”雖然沒有什麼明確地意識，不過蒼的聲音也冷了起來，然而，既然如此——蒼心中一動：這人要對自己做什麼，都只要一句話而已；但是他對自己的要求，早就不止是一句話這麼簡單了吧？難道……蒼用指甲輕輕刺了一下掌心，提醒自己，怎麼能夠恬不知恥的幻想棄天帝會用對待朱武的態度對待自己，如此做法，他只是因爲答應了自己的那個要求而已吧。

“聰明！所以，你要聽話。”

“……蒼聽說，二樓的客人已經離開了。”

“嗯？你還想他？”眉毛微微挑了一挑。

“客房已空，蒼再留此房間內不妥，還是搬回吧。”無視對方言語中莫名其妙地低劣挑釁，蒼不緊不慢地說着自己的心思，同時站起了身。

看着對面的人，棄天帝的下頜揚了起來，“公館不是任人挑揀房間的旅店。”

“蒼失禮。”對面的人已經走上，蒼現在已經知道，既然露出了這樣的眼神，就代表那個人是不會再聽任何解釋。

“還是……”盯着對方，本來讓人覺得也許是在生氣的表情卻突然漾起了不懷好意地微笑，“……擔心有人半夜溜上床？”

蒼嘴動了動，偏過頭去，“……是。”

其實很想大笑，然而看到面前微微偏轉的臉上努力壓抑的羞憤難堪，心卻突然軟了。棄天帝慢慢擡手，本想用手指，不過在觸碰到對方微微顫抖的身體之前，還是展開了手掌，用掌心攏起着那圓潤的帶着涼意的臉龐，略微眯了眼睛，沉聲說：“不用擔心。”聲音頓了一頓，卻也沒有給聽話者留下什麼喘息地空間，就繼續說：“我要，就堂堂正正地抱你上床。”

蒼真地是深深地吸了口氣，這態度和這話語讓他的腦海竟是一片空白，等到恢復了思考的能力的時候，對面的人已經不見了。

……

“唉呀！這是什麼花色啊！你們真是……不是說了，斷長官定的布料要單放的麼！”

瑞蚨祥掌櫃的看着被打開地油紙內中竟露出金紅顏色，頓時急得直搓手。這時，已經伺候完兩位好脾氣卻又不耐煩的少爺；戒神老者去請蒼，棄天帝便在這個當口走了進來，命令他們將帶來的布匹攤開，認真挑選花色。

“嗯？”看到那有點鮮豔地過分的顏色，棄天帝也不禁側目。

“長官恕罪，長官恕罪，斷廳長喜事將近，在小店定了喜服喜帳等等，因此最近新進了一批這樣的花色……想來是夥計拿錯了，長官恕罪！”掌櫃連連作揖，同時將那背錯布匹的小夥計的頭也一併按得深深低下，額頭簡直就要碰到膝蓋了。

慢慢點了點頭，不再說什麼，轉頭繼續審視面前攤開的十數種素雅的花色錦緞。

“長官，這眼看要入冬了，您自己不添件大衣麼？店裏去關外上貨的夥計便要回來了，少不得上好的皮草，屆時挑上好的給您送來如何？”

“好啊。”棄天帝隨口回答，只是心思明顯還在面前的布料上。

<br/>

時間意外得過得久了一些……

<br/>

“棄長官……”正在暮色中仔細觀察布料上的暗紋，身後傳來的那個聲音雖然輕飄飄地卻又字字清晰。棄天帝慢慢擡頭轉身，輕鬆隨意地表情，卻被蒼臉上出乎自己意料之外的嚴肅感染消失了，收了笑容，迎上那第一次毫無閃爍直接與自己對視的眼睛，方才那番話對他彷彿完全沒有效果，反而是眼中露出了與之前的閃爍於苦惱截然不同地堅定。

“棄長官，蒙您擡愛。收留小民已是感激不盡。因此，其他惠賜蒼不敢受了。”

眉梢微微抖動，棄天帝鬆開了手中的布料，靜默了一會兒，毫無語氣地說：“你們，先出去。”只是眼光仍是停在蒼身上，倒叫周圍愣住的裁縫夥計遲疑了一下，才忙不迭地退出去。

緩緩轉身，擡起鶴立雞群的長腿輕描淡寫地跨過了在地上橫陳的幾匹布，見到那人漸漸繃緊地臉，棄天帝停下腳步說：“我是哪一句話說得不明白？”

“……棄長官的好意，蒼明白。”

“我對你沒有什麼好意吧？難道你，覺得我，像無知青年那樣，用微不足道的禮物，表達，什麼膚淺地意思？”

“……對棄長官而言只是微不足道，然而對蒼而言已經是奢侈之物，蒼不能收。”

“那麼，你要忽視我的意見？”

“……蒼是小民，不能站在棄長官您的立場判斷。”

緊抿的脣微微一張，發出了只有自己能聽到地“嘖”的一聲，略揚了揚下巴，然而異色的眸子還是向着眼角滾過去，看着終於微低着頭的蒼，“如果我說……這只是表示你，對我稍微有點用途的謝禮呢？”

“長官和蒼之間只有交易而已，蒼並不是心甘情願地協助。”慢慢地搖着頭，卻又不自覺地垂着手臂，將冰涼的雙手交握在身前。

“你……有的時候很不討人喜歡。”

“不會討好長官，是小民愚笨了。”

“我不需要。”

“……長官的心思，恕小民無法理解……”眉頭微微蹙着，其實始終無法摸清他的意圖，然而這次似乎又有了點些微不同。

<br/>

面無表情地閉了一下眼睛，又再睜開，棄天帝突然自嘲地嘿然一笑，隨後擺正了姿勢，彷彿是在揭露一件什麼不可思議的事實一般，聳了聳肩，說：

<br/>

“我喜歡你。”

<br/>

“……長官亦是值得蒼敬重的長輩。”臉漲得通紅，蒼沉默了半天，才慢慢回答。

“傻孩子！”瞬時一股熱流衝到腦海，又下行灌注胸膛，棄天帝眼前一白之後，跨步衝了上去。

<br/>

<br/>

“尊管，這位小先生究竟是長官的什麼人啊？”

在三層的走廊裏等了良久，瑞蚨祥裁縫和掌櫃倒是不敢偷聽屋內對話，只是坐在臺階上無所事事地發木，突然看到剛剛見過的朱武少爺穿了件便裝急匆匆的下樓去了，才藉着給少爺讓路的混亂，湊近了問一直在一旁有些愁容的戒神老者。

“是……少爺的朋友。”戒神老者話音剛落，卻聽砰地一聲，活動室的大門已經被踢開，怒氣衝衝的Ｊ城鎮守使打橫抱着漸漸開始掙扎的蒼，幾步便穿過走廊。走進起居間之前，突然停步，說：“料子不選了！所有花樣都做一身！”隨後，在衆人瞠目結舌之前，已經堂而皇之拐入自己的臥室了。

<br/>

“長官！”被甩在那寬大的軟床正中間，蒼還沒掙扎起來，就又被結結實實地壓着了。

“動！再動！”狠狠地分開推在自己胸膛上的手臂，按在對方驚慌失措的臉旁，“難道真要像第一次那樣用手銬？”騰出一隻手，開始解開對方的衣釦。

“長官……”蒼的身體顫抖起來——從那帶着些血色的異色雙眸中能看出來，棄天帝這次是……

“我這次是真的……真的想要你……”恐怕身下人自己都沒發覺，棄天帝略微停了停手，摸了摸他緊繃地臉，“決定對誰怎麼樣，就一定會做到，和任何承諾交易都無關，這樣說，明白了？”

“長官，蒼並不值得……”明明渾身都在顫抖，蒼卻還是儘量強調這件自己覺得理所當然的衡量。

“怎麼對你是我的事。當然，怎麼看自己也是你的事……但起碼別輕賤自己。”對方的反抗漸漸小了，棄天帝的動作和語氣也溫柔了。

“除了長官，只怕沒人……”這話……聽在耳朵裏，竟是一陣戰慄之後的溫暖，蒼突然就不知道該不該繼續反抗，緊張過度之後，腦中所想，竟就不受控制的飄了出來……

“不是還有閻王鎖麼？”棄天帝笑了一聲，然而已經幾乎貼上的兩張面龐同時一變。

“……那不是長官你……”蒼的眼睛竟然睜得大大的，看着有些懊惱地偏過頭去的棄天帝，瞬時明白了，“……長官，你……你爲什麼要那麼說……”

“……我什麼也沒說過，什麼也沒承認過，是你自己非要這麼認爲……”竟有些像個小孩子一樣嘟嘟囔囔起來，棄天帝分開了蒼的襯衣衣襟，將臉埋在了他的肩窩。

“唔……”對方滾燙的脣滑過胸口，蒼本能地呻吟一聲，隨後才顫顫巍巍地說：“抱歉……蒼……誤會了……”心中卻舒坦了。

棄天帝“哼”了一聲，脣滑過蒼的鎖骨，仍是孩子氣地嘟囔：“被你誤會還少麼？”不過不等蒼回答，已經又擡起頭來，眼神中帶着笑，又變成了得意，“你是我的……我怎麼會叫別人碰你？”伸手解開蒼腰間的皮帶，穿過幾層覆蓋伸手進去，握着那顫顫發抖的玉柄如意，將大拇指按在頂端的雲頭上，輕輕摩挲，其他的手指也正要動作時，手腕又被人哆哆嗦嗦地推上了。

“長官，這種事……我自己來吧……”蒼咬着脣，臉上泛出紅暈來。

“哈……需要的時候我會要求的。”因爲笑，手微微抖了起來，也就沒有想停下的意思了。

……

“唔……”

“不知道男孩子第一次疼不疼，不過，我會盡量小心……”慢慢翻轉那已經被汗水溼潤漸漸軟癱地身體，讓他側身躺在自己身前，將手上沾着的抹在他兩腿之間的深壑裏，棄天帝自己也俯身躺了下去，曲起膝蓋插進本能併攏的雙腿間，同時舌尖滑過埋藏在柔軟髮梢中的耳根和耳廓，微弱的“嗯”聲和深沉的喘息漸漸變大了……

<br/>

<br/>

“是這裏了吧……”跳下洋車，看看緊閉院門旁邊的門牌，朱武又從懷裏掏出蒼寫給自己的新住址，確認一下，才走上前，一長兩短，輕輕敲了敲門。

敲過了良久，門才慢慢地被推開了一個小縫，一個三四歲的小女孩怯生生探出頭來，仰頭看了看衣冠楚楚的朱武，睜大眼睛打量了打量，才問：

“大哥哥？你找誰？”

“呃……小妹妹，這裏是……”半蹲下身，看着那毫不遜色洋貨店櫥窗裏洋娃娃一樣可愛的小臉，朱武臉上也堆出了自以爲親切地笑容。

“咩咩！誰叫你開門的！”天草和伊達突然衝了過來，趕緊將這個才搬來的小妹妹擋在身後，天草低頭說：“咩咩，不是說過了，師父師叔不在不要開門！”

“咦。是你？”伊達倒是大約記得朱武好像是戲班子的熟人，站在門口想了一想，等到人出來的時候，才懂得慢慢退後了一步，似乎是要將門關上。

“伊達，不認得我了？”朱武急切地踏上一步，手指已經扒上了門邊。

“喂，你幹嘛？！”

一聲大吼，腦後生風，朱武嚇了一跳，想要躲時，肩頭竟已被那人抓着，他一下掙脫不開，那一拳便結結實實砸在左眼上。只是這一下，朱武“嗡嗡”作響的腦海中已經有了判斷：此人絕對是打架高手。

“住手啊！”後面追來的紫荊衣和從屋內跑出來的赤雲染同時喊了一聲。

“啊？這個人趁咱們不在……”

“這是，蒼班主的好朋友……”不知如何介紹，紫荊衣看着捂着眼睛一聲痛哼蹲在一邊的朱武，心中先是本能的關心了一下他的傷勢，不過憂慮的重點馬上就轉移到了得罪了此人，又將如何收場的事情上，臉上也露出了些許厭煩地表情了。

“啊！？”孽角慌忙撒了手，扶起眼前還有些發黑的朱武，“抱歉，抱歉，我是新來的，不認識您。”

“孽大哥，快扶朱武少爺進來，用涼水敷一下……”赤雲染眼看着朱武的眼睛已經腫了起來，慌忙讓開了路，大開院門，招呼衆人先進來。

孽角也不待對方回答，便一手扶着蒙燈轉向的鎮守使少爺，另一隻手扶着又要關上的門，將眼前還冒金星的朱武帶進了院子裏。

<br/>

……

“多謝……”

今日上演全本【失·空·斬】，搬運時不小心，將幾件道具弄壞了，開演在即，衆人皆忙只有暫時不要登臺的紫荊衣和孽角有空，便叫他二人結伴回來取備用之物。兩人無話，拐過巷子口見到一人穿得輕浮正要闖進院子，孽角心急便直接衝了過來。此時，聽見從自己手裏接過另一塊冷毛巾的朱武腫着眼睛卻還道謝，孽角兇惡的臉上也露出了十分的尷尬。而聞訊聚集在正房廳堂內的其他人，已在紫荊衣的帶領下，扭頭竊笑了。

“朱武少爺，我們還要趕回戲園子，您……先在炕上躺躺，等散戲回來，再向您賠罪！”也沒多想，就將朱武扶進了自己和赭衫軍合住的房間，孽角覺得自己炕上髒，便抖開了對面赭衫軍疊得整齊地被子，讓晃晃悠悠口中不知喃喃自語些什麼的朱武躺下。

“你……”赫然聽說今晚上演的竟是【失·空·斬】，憋了十幾天的戲癮一下子犯了上來，朱武是很想說：我和你們同去戲園子的。然而此時躺在炕上，還覺得天地旋轉，眼睛是睜不開了，耳內也嗡嗡作響，估計去了也是白白浪費茶葉，也只好擡手揮揮，昧着良心說：“我沒事，有云染照顧我，這位大哥你先去忙……吧。”

……

“朱武少爺……”

散戲回來，站在自己炕頭，彎腰看着一手按着毛巾捂在眼上睡得迷迷糊糊的朱武，赭杉軍臉上也是難得一見略顯無奈的表情，“您……來此有何貴幹？”下午才見過伏嬰師，已經得知了蒼的境況，總覺得才短短几個鐘點，不至於發生什麼大事，朱武的來意倒是詫異。

“哼。”已經睡了好幾覺的朱武只覺得腦筋發木，聽到問，才勉強睜開右眼，只見點了兩盞燈的屋內已經擠了很多人，都圍在床邊看着自己，倒有點不好意思了，翻了個身，坐起來，倒是沒有忘記從被壓皺的外套口袋中取出一個信封。

“蒼讓我來給你捎個信，他正在我家養病，父親堅持要他痊癒才能離開，因此，可能還得再有一週吧。一切都好，讓你們不用太過擔心，那信封裏，是蒼給赭大哥的……”

“多謝。”接過來沉甸甸的信封，似乎有什麼硬東西，赭杉軍心思全被狼狽的朱武佔去，也沒多想，便當衆倒了出來，不料落在掌中的竟是自己那以爲早就不知失落在何處的玉佩。

“哎呀！”周圍的人都是輕輕一聲唏噓。

“哦，原來是這個……”朱武眯着眼睛看了半天，才一副明瞭地模樣解釋：“應是父親命斷風塵要回來的吧……前些日子，真是給諸位添麻煩……”看見此物，腦中便是一團混亂，然而他人無辜這點，倒是毫無疑問的。

“這玉佩可值錢了……”紫荊衣輕笑一聲，悄悄向着身邊的金鎏影說，“你說……睡一晚上兩百塊的話，這玉佩，少說也值個幾千了吧……”

“你說什麼！”朱武覺得自己被孽角打了那一拳之後，聽力一直有點異常，這句明明應是小聲私語，卻不知爲何自己卻聽得格外清晰刺耳，猛地一把抓下眼上的毛巾，睜着一對眼睛，在人群中尋找說話之人。

紫荊衣這話，其實赭杉軍和其他人也聽到了，只是自從來到Ｊ城之後，莫不是這樣地事情經的多了，這樣的話也聽得多了，便也知道反駁也好，憤怒也罷都是既無意義更是無力的做法了。

“你們……蒼不在，你們便讓他這樣胡說八道？”周圍氣氛不對，朱武更覺得難過，跳下床來，他和旁人不熟，便直接轉身向着赭杉軍了。

紫荊衣冷哼一聲，“是不是胡說，少爺應該比我們更清楚吧。”

看着衆人眼中帶着地並非是支持而是冷冷求證地神色，朱武只覺得整個腦袋都火辣辣地腫大了，怒說：“我當然清楚，父親除了第一晚，根本便沒再碰過蒼！”

“……！朱武少爺？”其實立在一邊，赭杉軍的手也早攥緊成了拳頭，“你……確定？！”

“當然確定！後來父親後來一直都在三層養傷，蒼是住在二層的客房，根本沒有接觸！後來戒老說了，蒼身體不好，父親也忙……”

“不是後來……朱武少爺，您真地確定……令尊他第一晚對蒼……做了那樣的事？”

“啊？這……”一時語塞，眼前所見千真萬確，只是心中縱有一萬個肯定，然而面對的是蒼親如家人一樣的同門，卻又叫他如何啓齒，況且，問話人的語氣更是啓他疑竇，朱武終於難得遲疑思忖，“赭大哥，你爲什麼這麼問……”

“……因爲，有人曾對我說：那幾日的種種，乃是令尊和蒼做給您的一齣戲……”雖不知說出口，會導致何等影響，然而事關蒼的名聲，這番話在赭杉軍心中憋了許久，如今也只有說了。

“……這……”

“……他分析得有理有據，不由人不信；而我亦曾依他之言私下用言語試探過蒼，看蒼的反應，只怕九成是真。”這話，其實不是說給朱武，乃是說給在場所有人聽了。

“赭師哥，你爲何不早說！”黃商子突然冒出了一句。

“……這也是那人所託……”說到這裏，赭杉軍只覺得有些羞愧違約，然而也只有原話轉述：“……既然是鎮守使授意做戲，自有他的目的，旁人拆穿，恐怕再生枝節，若是惹得長官不快，蒼所受的委屈也便毫無意義了……”

“哈，哈……這便好了，我一直不敢相信，蒼師哥能遭遇了那樣的事，這便好了！”黃商子咧嘴一笑，衆人自從到達之後一直鬱郁地臉上，神色也是舒緩起來。

“所以……朱武少爺，所以我才問您……真的確定……”

“我……我……這是誰對你講的？！”朱武突然雙手抓着赭杉軍雙肩，臉上的表情因爲高高腫起的眼眶，也看不出是狂喜還是急怒，竟是有點顯得猙獰地大吼起來。

“是閣下表弟……伏嬰師……閣下大可找他求證，同時請替赭杉軍致歉，雖曾答應守口如瓶，然而事有輕重緩急，赭杉軍違約了。”心中懊惱更深，竟是沒來由地一個寒顫，唯願自己所爲不會影響他之仕途。

“好，好！我去問他，我……多謝，多謝！！”連說兩聲多謝，朱武已是迫不及待，分開尚有些錯愕尷尬的封雲社衆人，衝出去了。

<br/>

“少爺……您！”

滿頭大汗跑來開門的戒神老者見到朱武，頓時嚇了一跳，然而還沒問出口，朱武已經推開他，跌跌撞撞地跑上樓去。

沒找到伏嬰師，便乾脆直接跑了回來。這個時候已經過了半夜，然而滿腔迫不及待的心緒卻讓朱武絲毫不覺得睏倦。跑上三樓，三步並作兩步衝進房門虛掩、內中還是明亮的Ｊ城鎮守使的起居室，奔入房間打開的主臥室門，卻見內中只有兩個傭人正在打掃，床單被褥堆了滿地，他左眼已腫，單用右眼早就酸澀的很，也不願細看，直接向着目瞪口呆僕人問：“父親呢？”

“老爺在對面臥室……”一個僕人擡手怯生生地一指。

“好！”朱武一個轉身，顧不得腿在起居室的茶几一角撞得生疼，又推開了對面臥室的屋門。

……

“父親！”滿懷驚喜地叫了一聲，把全屋的人都嚇了一跳。

站在床邊的棄天帝慢慢轉頭，看到兒子的樣子，也彷彿受了點驚嚇地揚了揚眉毛壓驚，隨後微微一笑，又向着坐在床邊椅子上，正給床上的蒼診視完畢的德國大夫，用漢語說：“ 用漢語說：“……嘖，您帶了消腫的藥麼？”

“嗯？”德國大夫聽不懂漢話，旁邊作爲翻譯的助手也不知道應不應該翻譯。好在大夫看了一眼鎮守使少爺那顯而易見地傷情，無奈地嘟囔了一句什麼，便叫助手從藥箱取藥了。

“父親……我有事要問你！”還是帶着滿臉興奮，大約因爲熱血沸騰的關係，只覺得左眼腫脹得更加難受。此時，那大夫的助手過來，怯怯的說：“少爺，您先上藥吧。”

“出去上藥。”棄天帝轉過頭，顯然是實在不想再看兒子那張久違的慘不忍睹的臉。等到助手拎着藥箱和朱武出去，才慢慢地再次俯下身，看看躺在床上臉色蒼白呼氣短促的蒼，扭頭，用自己的祖母教會的西文語言問：“情況如何？”

“你們究竟做了什麼？”德國人一臉認真。

不知如何回答而且其實也確實沒學過如何如實表述方才的情況，棄天帝看看勉強睜眼，一臉問詢表情地蒼，竟是忍不住“噗”地一聲笑了出來。

<br/>

<br/>

“嗯……長官，我……不太舒服……咳咳。”

幾番激情過後，棄天帝將懷中人翻過來仰天躺着。這時其實天已經黑了下來，然而陷在凌亂的被褥中渾身汗溼微微顫動的胴體那隱隱約約的輪廓卻好像瑩潤得能夠發出朦朦朧朧地微光來一樣，棄天帝帶着意猶未盡地表情，一面欣賞一面略作喘息，慢慢分開了那兩條本來微微扭在一處的雙腿，再度壓了上去。

“咳咳……”早已經一點力氣都沒有了，然而在他人的擺弄下，冷下去的身體又再度發燙，蒼的眉頭微微蹙緊，胸口卻毫無先兆地一陣發緊，被石頭壓着喘不過氣地感覺叫他悶聲咳嗽，彷彿只有借這咳嗽震動胸膛，才能幫助那似乎也無力再流動的血液再次動作。眼前陣陣發黑，在對方火熱的胸膛摩擦之中，竟是打了一個寒顫。

“冷？”棄天帝幾乎是覺得不可思議，然而懷中的身軀又是一陣寒顫和暗咳，隨後“啪嗒啪嗒”幾滴冷汗已經順着額頭落在枕頭上，自己的身體還有一部分在對方體內，當然感覺得到一陣陣地緊縮和顫抖，越發覺得不對，激情也減弱了不少，慢慢褪出來，然後打開了床頭的檯燈。

“不舒服？”跳下床，抓起地上的襯衣披在肩頭，撿起被子，蓋在已經縮作一團的蒼身上。

胸口悶得厲害，和那天在曌雲裳家裏徹夜唱戲之後的感覺相似，蒼幾番努力，又咳嗽了幾聲，眼前陣陣發黑只能搖了搖頭，竟是說不出話了。

“戒神！戒神！！”一面手忙腳亂地直接將長褲套上，一面便這樣衣冠不整地走出屋，“去接醫生過來！！快！”

<br/>

“我中午已經說過，不要做太過劇烈的活動，爲什麼這麼不聽話？體力透支，沒恢復之前，很容易導致心臟的負擔太大。”一天之內被請來前兩次的大夫顯然心情不太好，等不到回答，便想起這病人好像是中國歌劇的演員，大約是練功過度了吧，便又轉身，也不管對方聽得懂不懂，嚴厲地說：“你的心情我能理解，然而也要量力而行，不聽醫生的話，後果是很嚴重的。”

“大夫說，你其實還未恢復，這次是運動過度了。”棄天帝翻譯醫生的診斷，告訴在床上的雖然聽不懂，卻又被醫生這表情嚇住的蒼。

“啊？”通身發冷中，面頰卻是刷的一熱，胸口又是一陣發悶，不受控制的再咳嗽兩聲。

“放心，他以爲你偷着練功……”輕輕拍拍蒼的後背，棄天帝一本正經的眼神身處帶着深深地，只有蒼看地懂的得意和狡黠。

“剛才我聽了一下，有些心律不齊。不過總還年輕，躺一晚上應該沒事。”看見病人若有所思地點了點頭，大夫以爲自己的意見已經被完全轉達，便轉身對着彷彿家長一般的Ｊ城鎮守使繼續陳述，“不過……短時間內體力就消耗得這麼厲害好像不太可能，這個病人的體質也許有些特殊，爲了安全，我建議晚上有人陪着他一起睡覺，再有變化隨時叫我。”

“好。”看着醫生收拾器械，準備離開的架勢，棄天帝也站直了身體，陪着大夫出門的時候，突然問了一句：“……那，能洗澡麼？”

“不可以！洗澡也很耗費體力的活動。”大夫似乎是有些不高興地回答，隨後走出次臥室，走向正坐在起居室大廳沙發上讓助手笨手笨腳地上藥的朱武，只是低頭看了看，說了一句什麼，略微指導，待到處理完畢，告辭離開了。

<br/>

“父親！”等人都出門了，朱武才猛地想起來意，看着正要轉身走回臥室的棄天帝。

“……嗯？”手停在門把手上，似乎是有點疑惑地看着不知道哪根弦搭錯了，將這幾聲“父親”叫得又親切又熱情的兒子，說：“天晚了。”

“父親，其實你不用再和蒼一起……”

“哦？你要代替爲父遵醫囑陪病人睡覺？”

“不……不是啊。”

“那回房去睡。”不知道爲什麼，看見兒子那淤青的眼睛就有點煩，然而看見朱武轉身的時候，突然又問：“……對方如何？”

“啊？”

懶得再次解釋，棄天帝擡手指了指自己的眼眶。

“……是個誤會，我沒有還手。”

“禁足令，不要再違反了。”棄天帝“嘖”了一聲，迅速地拉開門，走了進去。





