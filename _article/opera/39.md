---
author: 西陵歌者
title: 戲
lang: zh-Hant
date: 2010-06-22 00:00:39
layout: post
depth: 2
category: opera
---

### 第三十九場 街邊公寓



舊曆十一月十二，說起來也是平平淡淡地一天，只是到了下班時分，伏嬰師覺得今天難得被一大堆文件壓得老老實實的少帥又開始有些躁動起來。

“表兄有事的話，剩下的工作交給我吧。”

“啊？”朱武睜大了眼睛，看着坐在自己桌子對面的辦公桌邊連頭都沒擡起來的伏嬰師，好像看着一個陌生人。

“怎麼？表兄不願意？”

伏嬰師等了一會兒，竟是沒有聽到什麼回答，有點詫異地擡起頭來。這時已經是傍晚時分，大約半個鐘點前熙熙攘攘的下班人流已經過去，現在棄家公館一層走廊這個角落有點冷清了。

“不……不是……”

朱武起身，開始收拾東西——如此慷慨，還是那個每天追着自己後面催着批公文的伏嬰師麼？他斜眼看了看，見對方起身去倒水，又趕緊將眼神收回，生怕他反悔。

“……已有風聲，蘇皖兩派的殘存勢力，只怕不日也要歸附廣州了。”伏嬰師捧着有點燙手的茶杯，悠然地說，“這樣的話，只怕再過半個多月……”輕輕閉了一下眼睛。

朱武正在門邊穿着衣服，聽到這句話，立刻怔住，想說什麼，卻又什麼也說不出來，最後只能深深地吸了口氣而已——其實這個話題，今天聽到並不覺得如何驚訝，只是帥府上下全都因爲那高深莫測的主人的緣故，對此事閉口不提。

“其實，大帥也一定同廣州方面接觸過了……少帥不知情麼？”似乎是休息完畢，伏嬰師睜開眼睛，扭頭看着還立在門邊沒有走出的朱武。

“……你的意思是，其實父親也……準備歸附了？”聲音有點澀澀，“背叛……北京？”

“沒有背叛那麼嚴重，這時局，投靠哪一方，只是押注而已吧。”伏嬰師一笑，“看看哪方更有利而已。非是小利，更是長利。”

“不會的。”朱武搖了搖頭，“父親不會背離兩位叔叔，更不屑和競天宗、玄貘那等未戰先降，卻又欺壓良善的怯懦小人爲伍！”

“少帥……我只怕，倘若南軍勝，留給大帥的只是識時務者爲方俊傑的風涼話而已。於你我看來，那等人的行徑只是懦夫趨炎，可是，百年之後，勝者爲王。而王者的歷史，縱使不屑，也不會把追隨自己的部下寫成一衆烏合的投機客吧？”

“……做好眼前事吧。”朱武的眼神定了一下，穿好衣服，“我約了人吃飯，做不完的事情，我回來自己會完成！”

<br/>

和伏嬰師的一番談話，讓朱武心中好像燃起了一團火，又彷彿擰了一個結，最近奉父命同東宮神璽打着各類年輕人玩鬧的旗號祕密接洽，私下的軍器往來，即便是初掌軍事的朱武，也知道這絕不是準備不戰而降的架勢。然而沿街所見所聞，盡是全國投降的新聞消息，卻叫他頗有一種欺世盜名甚囂塵上的憤慨了。故此，就這麼一路在微微的細雪中匆匆前行，卻絲毫不覺得寒冷。似乎眨眼間，竟就看見那棟磚木混建的公寓樓，自己熟悉的窗口亮着燈。

<br/>

雖然天已經黑了卻沒有合上窗簾，然而街道上之路燈稀少，卻也實在是看不清什麼。

“蕭……你真的已經決定了？”冷醉有些擔憂地問道，“怎麼對伯父說呢……”

“你不是也已經決定要去了？”蕭中劍靠着桌子站着，問得平靜，面孔卻又不由自主地轉向窗口。

“我不一樣啊，昨天五阿姨才給我添了個弟弟，我爹是顧不上管我了……但是蕭伯父只有你一個兒子啊……”

“國不立，何爲家呢？”蕭中劍不能不說心中一痛，然而輕輕吐了口氣，說：“況且，此時還需要策劃一段時日，我慢慢對父親講明也好，若能得到他的支持……”話到一半，便聽到“騰騰”的腳步聲上了樓。

“蕭！”

無論心情如何，見到蕭中劍的那一刻，朱武總是覺得一陣平靜和隱隱含在平靜中的快樂。

“蒼日……”身子本來靠在書桌上，因爲聽見聲音心內有數，蕭中劍也沒有動。而已經挪在門邊的冷醉，從走入的朱武手中接過門鈕，回頭說：“你們聊……我先回去。”眼神最後和蕭中劍互相交換之後，便關門而去了。

“蕭！”

屋內安靜了一下，朱武又叫了一聲。

“蒼日……我有點事要和你說……”

“先……出去吃飯吧……”一路跑來，朱武突然有點餓了，聽到狗不理在床腳邊喝牛奶的聲音，竟是一下子飢腸轆轆了。

“……噗……好。”蕭中劍本來一臉嚴肅，然而聽見這句話，竟也是禁不住笑了，點了點頭。

<br/>

<br/>

“伏嬰。”

晚餐是同棄天帝一起吃的，隨後便被叫了上來。此時，立在棄公館的書房裏，伏嬰師看着面前便裝的Ｄ省大帥，略微躬身，“舅父有何吩咐。”

“……明天去火車站接人。”

“嗯？”伏嬰師一愣，隨即回答說：“是……？”這個時間，這個地點，所接的應該不是公務之人了。

“嗯……我一個晚輩，和朱武的母親同姓，叫做挽月。”

“……那外甥應該稱她表妹？”

“嗯。”棄天帝低頭看看今天才收到的書信，“跑回來避難……”

“嗯……只是表妹一人麼？”

“其他的家人直接回魔界村去了，只有她，要先來Ｊ城看看我……和朱武……”

“嗯……”伏嬰師的眼珠轉了一下，“……舅父，即是如此，直接請表兄去……”

“不必了，朱武有自己的事情……你陪她幾天，就送回家便可。”其實，即使沒有手中那封姻親家長的書信，棄天帝當然也是明白對方一再強調來的乃是一位年輕、樣貌好、性格天真可愛又是在南方上過女學的大家閨秀的目的，更何況……“這是挽月的照片，你拿了去吧。”

“是……”伏嬰師走上幾步，捻起了被棄天帝推到桌腳的一張小照片，看了看，又點了點頭，說：“外甥明白了，挽月小姐在Ｊ城的這三天之內，定會好好照顧。”

“……甚好。”棄天帝點了點頭，又慢慢說：“……對補劍缺說：我要和蒼聊天。”

“是。”揣好了那不知是江西哪個小城照相館出品的濃妝豔抹的照片，伏嬰師略一躬身，退出了。

<br/>

今日回來，其實藺無雙和赭杉軍仍是照舊沒有敢給蒼安排，不過是在自己堅持之下臨時改了戲單子，加了一出罷了。蒼唯有苦笑，只覺得自己在同門眼中真的成了一個偶爾回來的票友了吧。然而才下了臺，便又見到等在後臺的補劍缺了。

“……嗯？現在？”此時已經是晚上十一點，邀請的理由也實在是太怪——或者說，棄天帝想要見自己根本就是不需要理由的吧。

“是……大帥今天吃完晚餐臨時吩咐的。請您……”補劍缺也是爲難，明明中午才目送這位離開，如今又要接回，此時後臺都以看着一種慾求不滿的惡霸家中走狗的眼神望着自己，他也只能禮貌再禮貌而已。

“我卸妝更衣。”不再多話，蒼的眉頭還是不察覺地蹙了一下，可是又實在想不出來，少言寡語的棄天帝口中所謂“聊天”究竟是要和自己說些什麼了。

<br/>

<br/>

“蒼日……”

吃過火鍋，朱蕭二人並肩而歸。

也不知是因爲酒興還是心內有事，兩人似乎都感覺不到什麼寒冷，也沒有急於回去公寓的心思，一路閒走，竟到了黑不見人的大明湖邊了。

“嗯？”

席間，彷彿是有了什麼默契一般，誰也沒有再提起自己心中最重要的正事，反倒是第一次這樣興高采烈地將相識到交往的短短几個月間的趣事一遍遍地回顧，再後來就不知道再聊些什麼話題。如今，看蕭中劍望着面前漆黑一片卻又泛着寒氣的冰凍湖面，似乎是真的要說上些許正事了。

“其實……我……”心中祕密太多，一下子將要開口，卻不知道先說哪一個了，最後猶豫一下，才慢慢說：“我打算過幾天，就和同學們一起，上京請願，要求北京交權給南軍。蒼日你……”

“不行！！”沒等對方說完，朱武震驚之餘，這兩個字幾乎是本能出口了，“絕不能去！”

“蒼日……”蕭中劍也是震驚了一下，看着如此堅決地朱武竟不知道說些什麼。

朱武腦子一團混亂，“不該去……不能去……總之，不許去……”

“蒼日！”其實本來……還抱着蒼日能否同行的想法，然而對方竟是反對地如此堅決，蕭中劍赫然覺得眼前發白，只說了幾個字，就滿身大汗了。

“太危險了！”朱武其實腦海中也是一片空白，然而隨父親在北京時，天者所說的話，其實一直是記在心裏的，“真的……太危險了！”

“如何危險？……”蕭中劍強將怒火壓下，皺着眉頭問道，只是還沒說完，朱武卻好像知道他下面的說辭一般，斷然搖頭，“北京和Ｊ城不一樣……”——因爲冷醉被抓去關了半宿，所以，蕭中劍參加學生請願並且自家窗戶被砸的事情，朱武是知道的，不過倒也不太在意——“你不懂，北京太危險！”朱武急得直拍額頭，“而且，根本不會有什麼作用……”

“沒做過，怎麼知道……如今的民衆就是都懷着這樣的念頭，才會在這劣境中隱忍，才會在這黑暗中沉默……”雖然朱武沒說什麼別的，但是那種“很多事你不懂”的態度，真的激怒了蕭中劍，“難道看透了這黑暗，見到了光明的可能，不應該振臂一呼麼……蒼日，廣州軍已經勢如破竹，和現在的政府不一樣，他們是有主義，有實力的，他們的軍隊不是私人的軍隊，是國家的軍隊啊，各地軍閥有遠見有良心的，紛紛歸附，難道不是大勢所趨麼？從辛亥年開始，軍閥混戰，民不聊生，如今即將統一，吾輩正該努力啊，蒼日，棄天帝和天者地者沆瀣一氣，頑固不化，已成……！”

“不是！”朱武也有點激動，“……你太過偏心了吧！區區說上幾句，你便信了？你對廣州瞭解多少？你又對北京瞭解多少？你對……對……Ｄ省大帥又瞭解多少？！無非是奪權，無非是征伐，什麼衆望所歸，什麼大勢所趨，只不過是軍閥混戰，先蹦出來的還是那些沒骨氣的小土匪和投機小人罷了！你怎麼能篤定，他們能給你一個更好的國家！”

“你不是也一樣不瞭解！而且，我雖不瞭解，卻願意參與，閣下又如何，軍校畢業，本應報國，然而閣下又做過什麼有利國家的事情了？！什麼都不做，只是一個一個的否定他人麼！”

“我……”朱武幾乎便要衝口而出：我乃Ｄ省少帥，是棄天帝之子……然而，被蕭中劍這一問，竟是一時噎住，雖然身爲少帥，然而仔細回想，竟是真的沒有做過什麼於家於國有利之事了，“但是，我……我一定會做……”

“好啊，現在就是機會啊！”

“不是……這不是你一定要做的，不值得犧牲的……蕭，以你的才華……”

“如果每做一件事都考慮值不值得，和市井商賈有何區別？你我學政法，懂軍事，難道就是回來做一個高等市儈麼！”

“不是……我是說，現在勝負難說……先照顧好自己不好麼……”

“蒼日，我看錯了你……不，我……本不該對你懷着什麼期待……”蕭中劍略微回頭，“你從來，就只是這樣一個……一個……”慢慢回想，對方真的只是一個莫名其妙混不着調的紈絝子弟而已吧……打動自己的，只有在青島海邊，指着他國國旗所說的那一句

“把這裏和那裏，都變回我們自己的地方！”這句話，就是說着玩的吧……

“蕭……”

“沒出息！”

“蕭，我……我只是不明白你爲什麼這麼偏向南軍……難道你的故鄉不是Ｄ省，不是Ｊ城？你爲什麼要生我的氣！”

“不是……我生你的氣，不是支持誰的問題……”其實說出口的話，便後悔，“……而是……蒼日，如果有能力，就要做更多更有用的事情，難道不是你對我說的？可是，我現在在努力，你呢？你又在做什麼？若說能力，明明是你更……”

“蕭……如果我支持棄天帝，支持北京呢？”

“支持他，你就替他賣命啊，幫他打贏這場仗啊……”寒風吹來，竟是通身一冷，打了個寒顫，蕭中劍頓時禁口。

“……蕭，北京不要去……”朱武的表情一下子平靜了，似乎不再爭執，只是苦笑着要求。

“恕我不能答應。這是我該做之事……請閣下也去做你該做之事吧！”

“……好，蕭……我不會讓你失望……也不會……唉……但是，去北京的事情，你一定要……慎重。”

“蒼日……”嘴脣動了動，對方早已經赫然轉身，似乎只能眼睜睜地看着那背影消失在夜幕中，攥緊拳頭，才發現手腳都凍僵了。

<br/>

蒼到達似乎永遠燈火輝煌的棄公館的門廳時刻，正是樓下客廳大鐘敲響什麼鐘點的時候。棄天帝已從三樓的書房，回到二樓的辦公室了，繼續工作許久了。

“長官。”

穿過半黑的走廊，進入了Ｄ省督辦的辦公室，先見坐在門口的祕書任沉浮和幾個小文書還在忙着，蒼有點不自在，覺得自己是不是還是直接上樓去等的好。

“坐那裏。”聽到腳步聲，便向着自己面前的一張沙發一指，棄天帝低頭在什麼文件還是公函上簽名，隨後親手封了，遞給走上來的任沉浮，繼續說：“送給暴風殘道，要快。”

蒼依言坐下，立刻便有騰出手的小文書識相地靜悄悄給他端來了點心和茶水。蒼點點頭，代替了說不出口的一聲“謝”。

……

時鐘滴滴答答地走着，一直靜靜坐在沙發中間，蒼終於忍不住靠倒在扶手上——棄天帝今晚實在太忙，批示各種文件之中，時不時有電報的往來，因此也一直沒有和自己說上一句話。可是，對方是會時不時看上自己一眼的，蒼很清楚。

這樣的在與己無關的忙碌中的安靜呆着，不多時便叫蒼昏昏欲睡。不受控制地漸漸閉上眼，隱隱約約聽到棄天帝終於吩咐：“給他蓋上……”然後過了一段時間，一條輕軟的毯子，暖暖的將自己裹住。而在朦朧中，不知是自己睡得熟了，還是周圍真的慢慢安靜了。

……

然而再次睜眼，諾大的屋內確實只剩下兩人了。

蒼坐起來——人少了，儘管生了壁爐，但是屋裏的空氣很寒——是被凍醒的，只能先繼續裹着那一直在辦公室備用的純毛的毯子坐一會兒。然而扭回頭，卻正好接上了棄天帝不知第幾次放下手中文件，看向自己的眼神。

“長官……我……去臥室？”說得有點結巴，但是蒼真的只是想說自己實在是困了。

“不。”收回了眼神，棄天帝掃了一眼桌上殘存的幾本公事，“等我。”

“嗯。”低了頭，蒼端起桌上的杯子，水不僅是滿的，而且還是溫的……蒼低頭喝了一口，身上漸漸暖了，就站起身，默不作聲地整理起自己趟過的地方了。

放下文件，看着對方立在沙發前慢慢疊起毛毯，棄天帝沉默了一下，說了一句，“書櫃裏有梳子……”便繼續審閱軍報了。

收好毛毯，又將沙發理平了，蒼慢慢轉身，卻見辦公桌後面的那人大抵是已經結束了所有的公事，看眼神的方向，應是對着自己，然而，這是第一次，蒼面對着棄天帝，竟不知道他究竟在看什麼了。

“蒼……”

“長官……”

……

“父親！”朱武猛地推門進來，第一次只是掃了一眼驚愕轉身的蒼，便直接跨步進來。

“什麼事？”棄天帝將已經前傾的身體坐直，看着臉上被凍得通紅，卻又好像還是熱氣騰騰的兒子。

“父親！我軍和南軍開戰，兒子願上前線！”

蒼的臉白了一下，張了張嘴，沒有說話，扭頭看着坐在椅上的棄天帝。

“……我會考慮。”嘴角微微抖動，最後還是翹了起來，棄天帝手指勾着書桌抽屜的銅把手，說：“你今天的工作……”

“我這就去！請您儘快考慮！”眼神堅定，看不見喜悅和興奮，可以說，完全和進來的時候是一個樣子。

“……做好三天之內啓程的準備。”棄天帝慢慢點了點頭，確實等到朱武快走出房間，才終於慢慢地補充了一句。

“是。”朱武回答了一聲，關門出去了。

……慢慢拉開抽屜，取出菸斗和洋火，擡頭，才看見蒼用一種爲難卻又關心的表情看着自己，棄天帝用沒叼着菸斗的那個嘴角笑了一下，拿起桌上的洋火，“過來，給我點菸。”

蒼慢慢走過去，接過了那盒洋火，卻是輕輕放在了桌上，“長官原諒，蒼不會……”

略微擡着頭，看着立在自己身邊的這個青年，棄天帝從嘴裏拿下菸斗，笑了——比起開始見面的時候，一點都沒變——伸手將蒼攬了過來，讓他坐在自己的腿上，雙臂環過去，把下巴輕輕放在那單薄的肩頭，“我教你……乾的菸草要填得緊些……”已經過了半夜，喉嚨有點幹痛，但是無所謂，仍是湊在他的耳邊一個字一個字地慢慢說……

“……我給長官您唱一折吧？”坐在那人懷裏，又聽到了似曾相識的心跳聲，蒼側頭看了看那其實是抱着自己還要點菸斗的人。

“不用。”淡淡的硝煙氣味，暗紅色的火光亮了起來，棄天帝吸了一大口，轉頭去吐煙。

“……朱武少爺……真的要上戰場了？”蒼想了想，既然這事不容迴避，那麼就坦然面對吧。

“他的責任。”菸草的效力慢慢發揮，腦筋不知道是情形了還是更加渾噩如醉，然而卻能促使聲音從容平穩了下來。

“嗯。”

“你怎麼想？”

在這個人面前，所有的欺騙即使是善意的安慰也是無用的，蒼猛地想起了那個“開戲”的上午自己那似乎不合時宜的心得，“蒼不懂軍事也不懂政治……沒有什麼想法。”

“安心。”拿開叼在嘴角菸斗，輕輕用舌尖撥弄着蒼的耳廓，竟是甜的。

……

“……我只是他的起點，卻做不了他的終點。”安靜了片刻，連菸斗裏的火都熄滅了，棄天帝才好像如釋重負一樣地說，然而一低頭，才發現大約是情緒所致，蒼竟已經靠在自己懷裏安詳地睡着了。

<br/>

回到臥室，替已經不願意睜眼的蒼換了衣服，蓋上被子，棄天帝坐在床的另一邊解開外衣的扣子……然而才摸到第二顆，便又想起從外廊上樓時，看到被樓下朱武辦公室的燈光照亮的雪地，猶豫了一下，又將釦子繫上，起身了。

其實，朱武已經趴在桌上睡了——雖然說着不用幫忙，不過伏嬰師並沒給他留下什麼需做的事情，然而因着那爭吵而起的衝動，卻叫朱武怎麼樣也不願意回屋去安逸睡下……迷迷糊糊趴在桌上，和各種人相處的種種，凌亂而混雜地涌了出來，彷彿做夢一般，聽到有人走近卻也不願理睬，直到那隻大手，如童年一般輕輕地摸了摸自己的後腦。

“阿爹……！”呢喃一聲，猛地擡頭，見那人真的站在身邊，朱武立刻起身，叫了一聲：“父親！”

“回屋去睡。”其實，立在邊上看兒子許久了，然而等到面對之時，卻只能說出這四個字了。

“父親……”欲言又止，卻見那高大身影已經轉身了。

“……換個房間，這裏不是適合你我談話的地方。”

……

一翻身，身邊還是空的，蒼睜開眼睛，天灰濛濛的有了點亮光，外面的起居室竟還亮着燈，卻又安安靜靜的沒有什麼聲音。翻身下床，推門去看，卻見父子兩人一模一樣的姿勢斜坐在沙發上雙雙睡着……





