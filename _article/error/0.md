---
author: kasana
title: 【ALL蒼】The Error
lang: zh-Hant
date: 2008-02-25 00:10
layout: post
depth: 1
category: error
subdocs:
  - title: 1. First Error (1)
    link: /error/1/
  - title: 1. First Error (2)
    link: /error/2/
  - title: 1. First Error (3)
    link: /error/3/
  - title: 1. First Error (4)
    link: /error/4/
  - title: 1. First Error (5)
    link: /error/5/
  - title: 1. First Error (6)
    link: /error/6/
  - title: 1. First Error (7)
    link: /error/7/
  - title: 2. Memory (1)
    link: /error/8/
  - title: 2. Memory (2)
    link: /error/9/
  - title: 2. Memory (3)
    link: /error/10/
  - title: 2. Memory (4)
    link: /error/11/
  - title: 2. Memory (5)
    link: /error/12/
  - title: 3. Salomé (1)
    link: /error/13/
  - title: 3. Salomé (2)
    link: /error/14/
  - title: 3. Salomé (3)
    link: /error/15/
  - title: 3. Salomé (4)
    link: /error/16/
  - title: 3. Salomé (5)
    link: /error/17/
  - title: 3. Salomé (6)
    link: /error/18/
  - title: 4. Moderato (1)
    link: /error/19/
  - title: 4. Moderato (2)
    link: /error/20/
  - title: 4. Moderato (3)
    link: /error/21/
  - title: 4. Moderato (4)
    link: /error/22/
  - title: 4. Moderato (5)
    link: /error/23/
  - title: 5. Endless Waiting (上部完)
    link: /error/24/
  - title: 1. To be by your side (1)
    link: /error/25/
  - title: 1. To be by your side (2)
    link: /error/26/
  - title: 1. To be by your side (3)
    link: /error/27/
  - title: 1. To be by your side (4)
    link: /error/28/
  - title: 1. To be by your side (5)
    link: /error/29/
  - title: 1. To be by your side (6)
    link: /error/30/
  - title: 1. To be by your side (7)
    link: /error/31/
  - title: 2. Castle on a cloud (1)
    link: /error/32/
  - title: 2. Castle on a cloud (2)
    link: /error/33/
  - title: 2. Castle on a cloud (3)
    link: /error/34/
  - title: 2. Castle on a cloud (4)
    link: /error/35/
  - title: 2. Castle on a cloud (5)
    link: /error/36/
  - title: 2. Castle on a cloud (6)
    link: /error/37/
  - title: N. 靜默
    link: /error/38/
---

#### 作者：kasana

原載於[http://www.36rain.com/read.php?tid=106201](http://www.36rain.com/read.php?tid=106201)

被雷所以雷

---
