---
title: 風姿花傳
layout: post
author: shiow
lang: zh-Hant
category: flowers
depth: 1
date: 2011-03-15 23:00:00
subdocs:
  - title: 起
    link: /flowers/0/
  - title: Chapter 1
    link: /flowers/1/
  - title: Chapter 2
    link: /flowers/2/
  - title: Chapter 3
    link: /flowers/3/
  - title: Chapter 4
    link: /flowers/4/
  - title: Chapter 5
    link: /flowers/5/
  - title: Chapter 6
    link: /flowers/6/
  - title: Chapter 7
    link: /flowers/7/
  - title: Chapter 8
    link: /flowers/8/
---

#### 作者：shiow

原載於[http://36rain.com/read.php?tid=85537](http://36rain.com/read.php?tid=85537)

是一個充滿什麼都有可能會發生的世界，打從某天帝下凡來胡搞瞎搞之後，整個世界就大約的定立了下來。

整個世界大約分爲幾個國家，某天帝雖胡搞瞎搞，但他也留下了一個國家，想到時侯會回來觀照一下，這是鬼之國，一個生產火力石的國家。

所謂火力石，是能源石的一種，各個國家有各國國家主要能源石，也是各個國家主要型成的重點。

各國之間的往來，全依靠商隊。

在鬼之國的隔壁，是玄宗國，是一個生產水力石的國家，全國有９８％都是老黃思想，也被稱爲道之國度。

在兩國的另一邊是學海無涯，是個充滿儒家思想的國家，但不明原因整個國家目前正處於動亂的情況。

當然還有許許多多的小國，就不多說了。（其實屁不出來…）

<br/>

總之…故事就是發生在這個世界裏…

<br/>

首部曲：許諾（吞螣）

二部曲：風姿花傳（棄蒼）

三部曲：千年之戀（太學主ｘ六殊衣）

<br/>

各篇可獨立觀看

本篇開始

---
