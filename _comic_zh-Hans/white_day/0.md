---
author: 三层梳妆台
title: 2024白情
date: 2023-07-16 00:00:08
layout: post
depth: 1
lang: zh-Hans
category: white_day
toc: true
---

#### 画师：三层梳妆台

---

### 预热

扯头发 先发一张预热下，草稿一大堆就是不知道能画完多少（）

<figure class="responsive-figure">
    <img src="../../../white_day/1709397502.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<br/>

### 性转现代

画完了有被迷到……

<figure class="responsive-figure">
    <img src="../../../white_day/1709775366.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<br/>

### 小龙人

<figure class="responsive-figure">
    <img src="../../../white_day/1710341654_1.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<br/>

### 天命

<figure class="responsive-figure">
    <img src="../../../white_day/1710341799_3.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<br/>

### 老年人日记

<figure class="responsive-figure">
    <img src="../../../white_day/1710341944_2.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<figure class="responsive-figure">
    <img src="../../../white_day/1710341945_2-2.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<figure class="responsive-figure">
    <img src="../../../white_day/1710341945_2-3.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<br/>

### 异国pa

<figure class="responsive-figure">
    <img src="../../../white_day/1710342021.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>

<br/>

### 老年人日记（2）

<figure class="responsive-figure">
    <img src="../../../white_day/1710342077.jpeg" alt="Your Image" style="width: 100%; height: auto;">
</figure>
