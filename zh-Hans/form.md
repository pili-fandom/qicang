---
title: 弃苍贴贴
layout: post
date: 2024-01-22
---

活动已经结束啦！

---

<!-- modify this form HTML and place wherever you want your form -->
<form
  action="https://formspree.io/f/xkndoode"
  method="POST"
>
  <label>
    36雨昵称（必填）:
    <input type="text" name="name" required>
  </label>
  <label>
    邮箱（必填）:
    <input type="text" name="email" required>
  </label>
  <label>
    备注:
    <textarea name="message"></textarea>
  </label>
  <!-- your other form fields go here -->
  <button type="submit">Send</button>
</form>
