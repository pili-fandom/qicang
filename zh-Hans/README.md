---
layout: home
title: 公开亭
longtitle: 公开亭
permalink: /zh-Hans/
lang: zh-Hans
comment: home
---

### 05/04/2024 粮仓周报

弃苍初见纪念日粮食[汇总页](april)，持续更新中~~

### 23/03/2024 粮仓日报

- [一千零一夜（番外二条漫）](1001nights_comic/0/) By 三层梳妆台

- [天命（修订版）](fate/00/) By 终朝采蓝

### 19/03/2024 粮仓日报

##### 近期新增：

- [下山·二](down/0/) By 三层梳妆台

- [老年人日记（图）](diary_of_the_elderly_fig/0/) By 三层梳妆台

### 13/03/2024 粮仓日报

##### 近期新增：

- [2024白情](white_day/0/) By 三层梳妆台

- [老年人日记](diary_of_the_elderly/0/) By 三层梳妆台

### 09/03/2024 粮仓周报

弃苍初见纪念日12h产粮活动招募中~~! 详情请戳[活动详情](april)

### 23/02/2024 粮仓周报

##### 近期新增：

- [遇龙](encounter_dragon/0/) By 三层梳妆台

- [燃冬](burning_winter/0/) By 三层梳妆台

- [相爱相杀表格](enemies_to_lovers/0/) By 三层梳妆台

- [小龙人·三](little_dragon/0/#三) By 三层梳妆台

- [很草的摸鱼（性转注意）·三/四](gender_reversal/0/#三) By 三层梳妆台

- [可拆卸叽叽（限制级慎入）](cat/0/) By 章鱼腿毛

### 14/02/2024 粮仓周报

##### 近期新增：

- [食色性也](food_sex_human_nature/0/) By 惘问

- [弱水](weak_water/0/) By 刻舟求剑

- [伊索尔德之梦](isolde_dream/0/) By dgrey

- [喜宴](banquet/0/) By dgrey

- [空间、系统与宿命](space_system_fate/0/) By 容予

- [烂桃花](peach_blossom/0/) By 三层梳妆台

- [青宫客相关](transient_visitor_related/0/) By 三层梳妆台

- [小龙人](little_dragon/0/) By 三层梳妆台

- [很草的摸鱼](gender_reversal/0/) By 三层梳妆台

- [清醒梦](conscious_dream/0/) By 在昆仑玉虚当咖啡师

### 道境新闻

![cang](../assets/img/cang.jpeg)

**震惊震惊震惊!!!** 数日前一妖道角经过魔界宫外，听闻宫内花丛中似有异声，竟发现异度皇后和一狂徒正颠鸾倒凤不知天地为何物。目击者表示，被发现时俩人还在花丛中大汗淋漓，异度皇后的紫色鸳鸯肚兜还挂在那狂徒的腰带上呢!!! 这究竟是道德扭曲，还是人性的沦丧？现在我们来采访一下当事人٩(✿∂‿∂✿)۶

当事人A：（淡定）子虚乌有。

当事人B：（皱眉）谁造的谣？污秽。

（图为当事人A日常生活照）
