---
author: 文心玲
title: 神乐
lang: zh-Hans
date: 2011-03-22 05:10
layout: post
depth: 1
category: music
subdocs:
  - title: 卷一：遗迹
    link: /divine_music/1/
  - title: 卷二：妖怪鸟
    link: /divine_music/2/
  - title: 卷三：微风
    link: /divine_music/3/
  - title: 卷四：海妖
    link: /divine_music/4/
  - title: 卷五：瘟神
    link: /divine_music/5/
  - title: 卷六：白蛇
    link: /divine_music/6/
  - title: 卷七：妖网
    link: /divine_music/7/
  - title: 卷八：蛋糕店
    link: /divine_music/8/
  - title: 卷九：活陨石
    link: /divine_music/9/
  - title: 卷十：海洋之心（一）
    link: /divine_music/10/
  - title: 卷十一：海洋之心（二）
    link: /divine_music/11/
  - title: 卷十一：海洋之心（二）下 
    link: /divine_music/12/
  - title: 第十二卷：海洋之心（三）
    link: /divine_music/13/
  - title: 第十三卷：海洋之心（四）
    link: /divine_music/14/
  - title: 第十三卷：海洋之心（四）下
    link: /divine_music/15/
  - title: 第十四卷：海洋之心（五）
    link: /divine_music/16/
  - title: 第十五卷：真假桂冠（一）
    link: /divine_music/17/
  - title: 第十六卷：真假桂冠（二）
    link: /divine_music/18/
  - title: 第十七卷：真假桂冠（三）
    link: /divine_music/19/
  - title: 第十八卷：真假桂冠（四）
    link: /divine_music/20/
  - title: 第十九卷：妖精的号角（一）
    link: /divine_music/21/
  - title: 第二十卷：妖精的号角（二）
    link: /divine_music/22/
  - title: 第二十一卷：妖精的号角（三）
    link: /divine_music/23/
  - title: 第二十一卷：妖精的号角（三）下
    link: /divine_music/24/
  - title: 第二十二卷：妖精的号角（四）
    link: /divine_music/25/
  - title: 第二十三卷：伊甸园
    link: /divine_music/26/
  - title: 第二十四卷：迷途的羔羊（一）
    link: /divine_music/27/
---

#### 作者：文心玲

一架古琴引发的惨案？

前提＆避雷针：某只文心萌的苍一向是谜城到天罪时期与之后的苍，是那位谈笑间百步制敌的苍，是那位不到最后一刻都不愿意杀死小金的苍，是那位面容严肃却会对车车老揶揄他时出其不意将他一军的苍，也是面对强敌不屈不挠以天下苍生为己任的苍，于是如果与看倌同好的理解不同，烦请绕道．

---
