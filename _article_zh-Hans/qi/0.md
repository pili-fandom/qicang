---
title: 家有仙七
author: Lamour
date: 2008-01-29
category: domestic_qi
layout: post
lang: zh-Hans
depth: 1
subdocs:
  - title: 1. 公用天井的坏处
    link: /qi/1/
  - title: 2. 黄历：出行不宜
    link: /qi/2/    
  - title: 3. 丢什么都可以，何以偏偏丢了这个……
    link: /qi/3/
  - title: 4. 不堪回首，不请自来
    link: /qi/4/
  - title: 6. 食Se，性也
    link: /qi/6/
  - title: 7. 小七
    link: /qi/7/    
  - title: 8. 早安，美人
    link: /qi/8/
  - title: 9. 看！灰机！
    link: /qi/9/
  - title: 10. 觅食行为
    link: /qi/10/    
  - title: 11. 初次见面,请多克制
    link: /qi/11/
  - title: 12. Travel
    link: /qi/12/
  - title: 13. Team work 什么的，最吵闹了…
    link: /qi/13/
  - title: 14. 冷空气
    link: /qi/14/    
  - title: 15. 新年快乐
    link: /qi/15/
  - title: 16. 喂!你们是来干活的!
    link: /qi/16/
  - title: 17. 神奇的画
    link: /qi/17/
  - title: 18. 神光
    link: /qi/18/    
  - title: 19. 脑震荡比骨折厉害多了
    link: /qi/19/
  - title: 20. 谈一谈
    link: /qi/20/
  - title: 21. 问琴
    link: /qi/21/
  - title: 23. 一犹豫成千古恨
    link: /qi/23/
  - title: 24. 起床
    link: /qi/24/
---

#### 作者：Lamour

---

> ##### 某蓝注
> 目录少两章，但内容好像是连贯的...
{: .block-tip}

---
