---
title: 质子
author: 西陵歌者
date: 2010-03-05
category: hostage
layout: post
lang: zh-Hans
depth: 2
---

### 第四章

　　“朱武侄兄，天魔山狭道以东的玄朝境内的军营布置吾已经全部画在此图之上了。从玄朝大营驾回来的战车也一并交你，下去仿造三百辆，给兵士做对战演习之用。”弃天端坐自从建好之后就没怎么用过的天魔宫银安殿宝座之上，从容吩咐，把手中墨迹未干的羊皮地图递给身边戒神老者，示意他交给银锽朱武。

　　还有些惊魂未定的文武本以为载著一车粮食白菜还有生姜和弦首回来的魔侯还要在别院的帐篷里温存半天，而况此时已经过了正午，即便有些事务，只怕也是明日再说。谁知道，一杯鲜奶还没喝完，便听到了王宫之内轰轰然击钟鸣典，召集群臣的声音。

　　“算天河，”弃天帝看著短得可怜的文臣队伍，点首唤位列第二同时也是末班的青袍大臣，“城郊水渠修建得如何？”

　　“禀大王，啊，不是，禀陛下，已经完工五成了。”算天河一时顺口，叫了一声之后才想起伏婴宰相临去时才逐一交给众人各自研读的朝典之仪。

　　“嗯，将水渠规模再扩大一倍，方圆二百里都要顾及。春天到来之前，一定要如期完工。”（弃天，你不会算数啊，方圆一百里到二百里是扩大一倍么？）弃天劈头盖脸丢下一句。

　　“陛下，陛下今冬命令开挖水渠已经用去了岁赋结余的一小半，如今若再扩大，资费更多，即便是今年的岁赋全部用上，也有困难，况且此时也找不到这许多人力施工啊。”

　　“孤王记得，每年例行都有一笔钱预留在铸造司吧，目下尚无战事，铸造兵器可以暂缓，将钱用来修渠；至于施工人力，便向王公大臣借啊，你们哪个手下出不起千把兵卒啊？把兵士集中起来，垦荒修渠！”

　　“大王！”暴风残道抢步而出，急道：“铸造刀剑事关国家安危，怎可随意挪用？！为国捐躯的将士，又怎能做种田挖渠的粗活啊？”

　　“暴风，孤王问你，倘若孤欲春季起兵，你可在入冬之前攻克玄朝？”弃天在宝座之上俯下身来，眯起一对眼睛紧紧盯著蓝颜大将。

　　“这……怎有可能？”

　　“哈，除非玄朝拱手相让，否则孤王也不能。那我再问你，入冬之后我百万铁骑人无粮食马无草料之时，玄朝大军有无可能念在上天有好生之德，等到春夏季我们有了吃的再来进攻呢？”

　　“这……怕是不太可能。”

　　“是啊，那你说，是刀剑重要还是种地重要呢？”

　　“末将愚钝！”

　　“孤王此次深入玄朝地界，见他兵营屯粮积草，堆积如山，动辄万石，这便是南朝耕种之利了。我族民虽骁勇，然玄朝治下，不算归附小国，连同魔国共有诸侯七个，江山何止千里万里，便是断风尘策马狂奔，只怕一年也跑不到尽头，不做持久打算，又如何可得天下？嗯，说到了断风尘，他人呢？”弃天突然一愣，难怪升殿之后，总觉得殿上有点冷清，一开始还以为是少了伏婴师的缘故，此时看来，武将一边，似乎空缺的竟还不止一人。

　　“启禀陛下，断天王应该是乔装混去边境征粮……”黑羽恨长风突然想起今晨曾经见过一身玄朝公子哥打扮的内廷大将军，赶忙出班奏报，可惜话音未落，身后已经是阵阵窃笑了。

　　“等他回来，告诉他，孤王我已经把足够弦首吃一冬天的粮食蔬菜运回来了，以后他可以暂时不去边境了。”弃天斜坐在王座上，从敌营跑回来的快意和震撼此时已经渐渐消退，如今冷静下来，扫过殿上众王公，突然察觉气氛不对。

　　“还有何事？”虽不想问，但是魔侯也有自己的责任。

　　朱闻苍日看看沉默不语的大哥，随后又看看尚有些迟疑的众人，一咬牙，迈步走至殿中，道：“王叔，臣侄有事启奏。”

　　“啊，苍日啊。”见到二侄儿，弃天才想起出走的公主朱闻挽月尚未找回，不由得心中有些愧疚，温言道：“挽月应该已经过了边境，她聪明伶俐，想来能照顾好自己，况且她是去找伏婴，两人会和之后，更不用担心了。”

　　“月妹任性妄为，擅离宫廷，倘有什么危险，自是咎由自取。”朱闻苍日不动声色回答道，“即便她能安然归来，也请陛下按照族规处置。”

　　“苍日，何须如此严格，挽月无非是少年心性，倒是我疏忽了，早知道便让伏婴直接带她去中原散散心的好。”弃天将后背靠在宝座之内，耐著性子回答，苍还在别院正殿帐篷之内裹著被子发低烧，让他时时悬心不已。

　　“陛下，魔族族规，故老相传，乃是我族安身立命，行军制下之基，岂能轻忽，朱武大哥自那日众人返回，便已经当众责罚了螣儿等人，身体力行，令人钦佩。”

　　弃天一愣——方才没有细看，此时细数下来，四枪之内，除了吞佛童子，另外三个都不见了踪影。他不由的一皱眉，道：“朱武，这是何苦，螣儿他们也是好心，况且……”他刚想脱口而出：老师曾说刑不上大夫，但是话到口边，却想起能听懂的人已经上路了。

　　银锽朱武此时方才出班，躬身道：“臣侄三个逆子，鲁莽行事，至令陛下犯险，三十鞭子已是念及年少无知从轻处置了。”

　　弃天不耐烦的摇了摇手，道：“既然是朱武你管教儿子，孤王也就不再多话了。”心头忽然一悸，刚要起身说退朝，朱闻苍日已经抢在他前面，道：“陛下，螣儿三人已经受到惩罚，吞佛童子因为陛下当日言之有功，便将功抵过；挽月他日回来，臣侄必定罚她；只是还有一人私闯边境，尚未处罚啊。”

　　“朱闻苍日！”弃天帝豁然站起，“你是要朕处罚老师么？”

　　朱闻苍日道：“陛下，伏婴宰相曾言：国事当前，岂论私交？九祸女首为了魔国安危，已经只身入朝为质，陛下有怎能因为数年照顾之恩，便对弦首如此姑息纵容呢？既然在我朝为质，便该遵从我国刑律，身为下囚，窃马私逃，乃是重罪，当处黥面刖足的极刑。”

　　“住口！老师乃是贵宾，怎们又是下囚了？！朱闻苍日，你简直……”

　　“苍日，那日宴前陛下曾有言道：一日为师终生为父，弦首也可算得是你我族人了，你用律有差啊。”银锽朱武微微转过半个身子，静静说道。

　　“大哥所言极是，既然如此，请陛下依照我族族规处置，不如便照著大哥惩戒螣儿，如法炮制，在金顶帐前当众鞭笞三十，以显示陛下铁面无私，处事公允。”朱闻苍日此时得了兄长撑腰，鼓起勇气正视魔侯那一对异色的眼睛。

　　“朱闻苍日！莫要忘了，若非弦首，只怕螣儿三人早就万箭攒身，死在天魔峡谷。”弃天身向前探，狠狠看著阶下众人。

　　“陛下，此事仍有疑点，臣侄正要启奏，弦首身在宫内，又怎会先于陛下知道螣儿出城之事？只怕逃亡是实，峡谷相救只是巧合。”

　　“一派胡言，老师若是逃亡，直奔萧关、蓝关而去便是了，怎会舍近求远，跑去原本无人的天魔狭道！”

　　“臣侄并未说完，天魔峡谷虽是要地，然而早在陛下为质之时，两国便已立约，绝不在峡谷驻军，那日玄朝军队骤然出现在峡谷之内，莫非是……”

　　“够了！”

　　“陛下曾入敌营，想来此事种种端倪，比臣侄更加清楚。”朱闻苍日又是一礼，躬身在大殿之上，等待魔侯裁决。

　　弃天几乎要将宝座扶手捏碎，然而苍老师无精打采叨念的为君之道言犹在耳，字字句句仿佛将他钉在宝座之上，沉静半晌，暴怒面容忽然归于平静，涩声道：“准苍日所奏，你们先往城外金顶宝帐，稍后……我便请老师前往受罚。”

　　……

　　看著自己话音一落，顷刻间便走得空荡荡的大殿，弃天帝坐在宝座之上一动不动不知多久。突然戒神老者慌忙跑了进来，“陛下，老仆方才看见众位王爷在金顶大帐之前搭起刑台，说是要鞭笞弦首，陛下，万万使不得啊，苍先生现今状况，如何挨得住啊，只怕是要被活活打死……啊！”自知失口，戒神老者慌忙把嘴捂住。

　　“戒神，随我出城吧。”弃天双目轻阖一声嗤笑。

<br/>

　　今日是冬季难得的晴天，走出城门之时，一轮血红残阳已经没入地平线小半，四下里金光一片。虽是如此，风却冷得怕人，刮在脸上有如刀割。

　　弃天帝没有骑马，独自一人缓步走到新搭起的刑台之前，饶有兴趣的看看结实的刑架，又望望帐口翘首以待许久，此时纷纷面现失望的众文武，突然一笑。

　　“陛下，请请出弦首。”朱闻苍日深深一躬到地。

　　“孤王说过，一日为师终生为父，我魔族又有父罪子受的习俗，弦首鞭刑，本王便按照魔族规矩，加倍代他受过！”话音方落，已经踏上刑台，肩头华美裘皮已然滑落，露出洁白无暇，肌肉贲起的上身。

　　“啊，这……”众人皆是一片愕然。

　　“行刑！”长袍坠地，弃天赤膊立在天地残阳和凛冽寒风之中，转过身形，双手一扶刑架横木，将脊背对著众人。

　　众人面面相觑，突然“啪嗒”一声，二指粗的牛皮鞭子落在地上，黑铁塔一样的刽子手“扑通”一声跪倒，叩首道：“大王，小人万死也不敢啊！”

　　“哼，你们谁敢替他行刑？！”弃天帝略微转头，眼光在众人脸上一一扫过，眼光之烈，连朱闻苍日都低下了头。突然，银锽朱武不卑不亢，缓缓迈出一步，从刽子手脚边捡起鞭子。

　　“……臣侄斗胆执鞭，以全陛下忠孝之名。”

　　“……”弃天自嘲一笑，“来吧，孤王欠你。”

　　“臣侄全家，纵使为我魔族粉身碎骨亦无怨言，断不敢做如此想。”朱武一面说，一面沿著木梯缓缓榻上刑台，话音落时，人已经到了弃天帝背后。“王叔，臣侄代王叔执行族规，得罪了！”说罢，更不迟疑，手臂高扬，鞭声连响，点点血花迸散长空，融入如血的夕阳残霞之中。

　　……

　　“快快。”戒神老者语带哭音，向著双目通红的面露凶光的补剑缺道：“先将大王扶入帐中，我回宫去取伤药。”

　　“哈，孤王十五岁上阵杀敌，身上未落一条伤疤，不想今日，却在自己族人手下见血了。”

　　“大王啊。”架著将手臂搭在自己肩头艰难拖步，虽然脸色惨白，嘴唇抖个不停却还要说笑的君主，补剑缺不知平日的蛮力都不知道哪里去了，区区几十步的距离，竟是怎么也走不到，手掌偶尔碰到弃天帝后背，触手滚烫，皆是片片血肉模糊，顿时如遭电击，猛地弹开，“都是自家人……这究竟是要干什么啊……”补剑缺只觉得眼中发热，却是腾不出手来抹抹有些湿润的眼角。

　　“狼叔啊，去把我的外袍捡来，现在的风可是有点凉了啊。”坐在帐中马扎上，弃天帝双手抓著面前条案两角，咬紧了牙关强撑住身体，寒风从帐篷缝隙之中灌入，寒气飞速划过后背伤痕，竟是如同利刃，阵阵锥心刺痛直冲脑际，眼前更是一片黑暗。意识逐渐模糊起来，只觉得血水灌入靴筒，脚下本已经冻硬的泥土似乎也被落下的热血融化，双足落处如同烂泥般虚软。

　　……

　　不知何时，一条温热的手巾在赤裸后背小心翼翼轻轻擦拭，疼痛舒缓，让弃天帝已被疼痛占满的脑筋渐渐有了几分空闲。

　　“……？”弃天帝的眼睑动了一下，这感觉既不是戒神或补剑缺，也不是那个……苍。

　　“换水。”一个沉厚充满磁性的声音冷冷的吩咐一旁的补剑缺。

　　“吞佛？”弃天帝身体一震。

　　背后的红发将军没有回应，专心致志处理眼前的伤口，既不邀功，也不解释。

　　“吞佛……是你命人先把四枪出城的消息告诉老师的吧？”弃天突然问道。

　　“是。”

　　“做得好啊……”听不出魔侯咬著牙说这句话究竟是想夸赞还是愤恨，只是，无论本意为何吞佛童子仍旧丝毫不为所动。

　　吞佛童子将手巾丢在一旁，洗去手上血迹，起身走向帐口，“至少今冬，不会开战了。”说罢，侧身让开了抱著药箱冲进来的戒神老者，头也不回的出去，走时随手将厚厚的帐帘放下。

　　“陛下，吞佛将军他……”戒神老者没料到此人会从帐中走出，顿时满脸惊慌疑惑。

　　“老头儿，快进棺材了？手脚这么慢！”看见老伙伴呆立不动，补剑缺气急败坏骂了一句。

　　“我绕道去见苍先生……”戒神老者话说了一半，赶紧住口。

　　然而，弃天原本低垂的眼睛，似乎向上挑了挑，但是还没看到将帐口遮了个严严实实的帐帘，就又垂下了，轻声问道：“老师……他怎么说。”

　　“苍先生……也很关心陛下您的伤势……”戒神老者低下头，避开弃天目光，将药箱放在桌上，掀了盖子几下，才发现方向反了，又慌忙调过来，将药箱打开。

　　“哈，戒老，这不是老师会说给你听的。”弃天一声轻笑，已从老仆的眼中看到了所有。

<br/>

　　夜深了，苍躺在榻上一动不动，微热的体温，注定今夜往事梦回，辗转难眠。朦胧之中，不知怎的，竟是反复想起十年前，那个人来到封云城，眼神仿佛一个被父母遗弃了的孩子，充满了愤怒和警戒；一个月后，他从认识不多的玄朝文字中挑了一个字作为自己的名字，那个字，是：弃；十年后，他走的时候，豪情万丈的签在通关文碟上的，是两个字：弃天。

<br/>

　　“哈，天未弃汝汝弃天，苍生有幸……苍……何幸啊！”
